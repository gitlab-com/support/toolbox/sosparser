import streamlit as st
from streamlit_option_menu import option_menu
from helpers.mutiNodeMetadata import *
from helpers.utils import *
from helpers.productionLogs import *
from helpers.gitalyLogs import *
from helpers.sidekiqLogs import *
from helpers.apiJson import *
from PIL import Image
from os.path import exists
import glob    

cli_init = False

def validateFilepath(file_, c):
    file_path = file_
    with c:
        for f in file_path:
            if exists(file_ + f):
                st.markdown(" Read file : _*" + f + "*_  :white_check_mark:")
            else:
                return False
    return True

def main():
    initialize()
    indexPage()


def indexPage():
    c1,c2 = st.columns([7,3])
    with c1:
        logo("\U0001F6A8 SOS Parser \U0001F5C3")
        st.markdown(
            """This page allows us to work with multiple log files at once. Simply provide the path of the root directory where the logs are downloaded and the tool will process them.
            <br>
            For more information on the tool, please refer to the project : [SOSParser](https://gitlab.com/gitlab-com/support/toolbox/sosparser)""", unsafe_allow_html=True)
        st.markdown("---",  unsafe_allow_html=True)
        st.markdown('<p class="font3">' + "Root directory path :" + '</p>', unsafe_allow_html=True)
        stb = st.text_input("Root directory path :", key="path", label_visibility="hidden")
        if st.button("Submit"):
            with st.spinner(text="Validating the folder content, processing..."):
                if validateRootPath(stb, c2):
                    st.session_state.root_path = stb
                    c2.text("Extracting tar files")
                    extract_tar_files(stb)
                    c2.text("Extracting tar files completed")
                    c2.text("Extracting Metadata from the log files")
                    generate_metadata(stb)
                    c2.text("Metadata extraction completed, check the table below : ")
                    showLogFiles(stb, c2)
                    st.session_state.meta_file= ""
                    st.session_state.meta_file_exists= False
                else:
                    c2.error("Something went wrong, please check file path", icon="🔥")
        else:
            if st.session_state.meta_file_exists:
                showLogFiles(st.session_state.root_path, c2)
        if st.experimental_get_query_params() != {} and st.session_state.cli_init == False:
            cli_args = st.experimental_get_query_params()
            root_path = cli_args['path'][0]
            st.write(root_path)
            c2.empty()
            if validateRootPath(root_path, c2):
                st.session_state.cli_init = True
                st.session_state.root_path = root_path
                c2.text("Extracting tar files")
                extract_tar_files(root_path)
                c2.text("Extracting tar files completed")
                c2.text("Extracting Metadata from the log files")
                generate_metadata(root_path)
                c2.text("Metadata extraction completed, check the table below : ")
                showLogFiles(root_path, c2)
                st.session_state.meta_file= ""
                st.session_state.meta_file_exists= False
            else:
                c2.error("Something went wrong, please check file path", icon="🔥")



def validateRootPath(root_path, c2):
    if not os.path.exists(root_path):
        c2.error("The path does not exist", icon="🔥")
        return False
    else:
        tar_gz_files = glob.glob(os.path.join(root_path, '*.tar.gz'))
        if not tar_gz_files:
            return False
        c2.success("Found these log files", icon="✅")
        for tar_gz_file in tar_gz_files:
            tar_gz_file = tar_gz_file.replace(root_path, "")
        return True

def initialize():
    st.set_page_config(
        page_title="SOS Parser - Multi Node",
        page_icon="\U0001F6A8",
        layout="wide",
        initial_sidebar_state="collapsed",
    )

    if "root_path" not in st.session_state:
        st.session_state["root_path"] = ""
    if "file_path" not in st.session_state:
        st.session_state["file_path"] = ""
    if "valid" not in st.session_state:
        st.session_state["valid"] = False
    if "meta_file" not in st.session_state:
        st.session_state["file_path"] = ""
    if "meta_file_exists" not in st.session_state:
        st.session_state["meta_file_exists"] = False
    if "cli_init" not in st.session_state:
        st.session_state["cli_init"] = False
    st.markdown("""
        <style>
        [data-testid=column]:nth-of-type(2) [data-testid=stVerticalBlock]{
            gap: 0.05rem;
        }
        </style>
        """,unsafe_allow_html=True)

    st.markdown(
        """ <style> .font1 {
        font-size:30px ; color: #FCE4D8;} 
        </style> """,
        unsafe_allow_html=True,
    )
    st.markdown(
        """ <style> .font2 {
        font-size:35px ; color: #FBD87F;} 
        </style> """,
        unsafe_allow_html=True,
    )
    st.markdown(
        """ <style> .font4 {
        font-size:50px ; color: #CCF5AC;} 
        </style> """,
        unsafe_allow_html=True,
    )
    st.markdown(
        """ <style> .font3 {
        font-size:20px ; color: #F75590;} 
        </style> """,
        unsafe_allow_html=True,
    )
    st.markdown(
        """ <style> .font5 {
        font-size:15px ; color: #3C787E;} 
        </style> """,
        unsafe_allow_html=True,
    )
    return True

def logo(text):
    cl1, cl2, cl3 = st.columns([10, 10, 5])
    st.write(
        "<style>div.block-container{padding-top:0rem;}</style>",
        unsafe_allow_html=True,
    )
    cl1.markdown('<p class="font4">' + text + '</p>', unsafe_allow_html=True)
    image = Image.open('static/gitlab_logo.png')
    cl3.image(image, width=200)

def showLogFiles(metadata_file, c_2):
    colms = st.columns((0.3, 3, 3, 1, 2, 1.5))
    fields = ['', "Hostname", 'Services', 'DB Migration', 'License', '']
    for col, field_name in zip(colms, fields):
        col.markdown('<p class="font3">' + field_name + '</p>', unsafe_allow_html=True)
    
    with open(metadata_file + "/metadata.json") as f:
        metadata_ = json.load(f)["nodes"]

    for x, entry in enumerate(metadata_):
        col1, col2, col3, col4, col5, col6, col7 = st.columns((0.3, 3, 3, 1, 2, 0.75, 0.75))
        col1.write(x)  # index
        col2.write(entry['node_name'])
        services = ", ".join([f"{service} ✅" if status == "run" else f"{service} ❌" for service, status in entry["services"].items()])
        col3.write(services)
        if len(entry['migration']) > 0:
            col4.markdown('<p style="text-align: center;">❌</p>', unsafe_allow_html=True)
        else:
            col4.markdown('<p style="text-align: center;">✅</p>', unsafe_allow_html=True)
        if type(entry['license']) is dict:
            col5.write(entry['license']['License valid from'])
        else:
            col5.write("Not available")
        url = generate_encoded_url(entry['path'])
        col6.button("Details", key=x, on_click=my_callback, args=(entry, c_2))
        render_button_link(col7, "Launch", url)


def my_callback(entry,c_2):
    st.session_state.meta_file= entry['path']
    st.session_state.meta_file_exists= True
    display_node_details(entry, c_2)

def display_node_details(node_details, column):
    column.markdown('<p class="font3"> SOS captured at : </p>',  unsafe_allow_html=True) 
    column.markdown(f'- {node_details["time_stamp"]}')
    column.markdown('<p class="font3"> Hostname :</p>',  unsafe_allow_html=True)
    column.markdown(f"- {node_details['node_name']} ")
    column.markdown("<p class='font3'> Log folder path on disk :</p> ",unsafe_allow_html=True)
    column.markdown(f"- {node_details['path']} ")    
    
    services = list(node_details['services'].items())
    root_path = node_details['path']
    mid_index = len(services) // 2
    column.markdown("<p class='font3'> Service status :</p> ",unsafe_allow_html=True)
    col1, col2 = column.columns(2)
    for service, status in services[:mid_index]:
        col1.markdown(f"- **{service}**: {'✅' if status == 'run' else '❌'}")

    for service, status in services[mid_index:]:
        col2.markdown(f"- **{service}**: {'✅' if status == 'run' else '❌'}")

    column.markdown("---")
    col3, col4, col5 = column.columns(3)
    col3.markdown("<p class='font3'>Memory </p> ",unsafe_allow_html=True)
    memory = node_details['memory']
    col3.markdown(f"- **Total**: {memory['total_mem']} MB")
    col3.markdown(f"- **Used**: {memory['used_mem']} MB")
    col3.markdown(f"- **Free**: {memory['free_mem']} MB")
    col3.markdown(f"- **Available**: {memory['available_mem']} MB")

    col4.markdown("<p class='font3'>Up time</p> ",unsafe_allow_html=True)
    uptime = node_details['uptime']
    col4.markdown(f"- **System**: {uptime['system_time']}")
    col4.markdown(f"- **Up Time**: {uptime['up_time']}")
    col4.markdown(f"- **Users**: {uptime['users']}")
    col4.markdown(f"- **Load Average**: \n {uptime['load_average']}")

    col5.markdown("<p class='font3'>Migrations</p> ",unsafe_allow_html=True)
    if node_details['migration']:
        for item in node_details['migration']:
            print(item['migration_id'])
            col5.markdown(f"-  ID: {item['migration_id']}")
    else:
        col5.markdown("- No pending migrations")    
 
    column.markdown("<p class='font3'>License :</p> ",unsafe_allow_html=True)
    if type(node_details['license']) is dict:
        license_info = node_details['license']
        for key, value in license_info.items():
            column.markdown(f"- **{key}**: {value}")
    else:
        column.text("No license info available")
    column.markdown("---")
    service_names = [service[0] for service in services]
    cx_ = column.columns(3)
    fig_prod, fig_sdk, fig_gitaly = None, None, None
    col_index = 0
    for service in service_names:
        if service in services_dict and checkFileExists(root_path,service):
            if service == "puma":
                fig_prod = getProductionLogDF(cx_[col_index], root_path)
            elif service == "sidekiq":
                fig_sdk = getSKDataFrame(cx_[col_index],root_path)
            elif service == "gitaly":
                fig_gitaly = getGitalyDataFrame(cx_[col_index],root_path)
            else:
                column.error("File not found", icon="🚨")
            col_index += 1
    column.markdown("---")
    if fig_prod != None:
        column.markdown("<p class='font3'>Production </p> ",unsafe_allow_html=True)
        column.pyplot(fig_prod)
        plt.close(fig_prod)
    if fig_sdk != None:
        column.markdown("<p class='font3'>Sidekiq </p> ",unsafe_allow_html=True)
        column.pyplot(fig_sdk)  
        plt.close(fig_sdk)
    if fig_gitaly != None:
        column.markdown("<p class='font3'>Gitaly </p> ",unsafe_allow_html=True)
        column.pyplot(fig_gitaly)
        plt.close(fig_gitaly)
    if checkFileExists(root_path,"API"):
        column.markdown("<p class='font3'>API </p> ",unsafe_allow_html=True)
        renderAPIGraph(column,root_path)
    column.markdown("---")            

def renderAPIGraph(column, root_path):
    log_file, debug = read_log_file_api(root_path)
    df = convert_to_dataframe(log_file)
    df = mapColumnsAPI(df)
    fig = plot_rps(df, "5 Seconds")
    column.pyplot(fig)
    plt.close(fig)
    del df
    return True

def getProductionLogDF(_column, file_path):
    try: 
        lines, debug = read_log_file_pr(file_path)
        df = convert_to_dataframe(lines)
        df = mapColumnsPD(df)
    except KeyError:
        _column.markdown("<p class='font3'>Something wrong with the log file </p> ",unsafe_allow_html=True)
        return None
    _column.markdown("<p class='font3'>Production </p> ",unsafe_allow_html=True)
    metadata = metadataPD(df)
    for x, meta_in in enumerate(metadata.keys()):
        _column.markdown(f"- <p class='font5' style='display:inline'>{meta_in}</p> : {str(metadata[meta_in])}", unsafe_allow_html=True)
    fig_prod = plot_rps(df, "5 Seconds")
    del df
    return fig_prod

def getSKDataFrame(column_sk,file_path):
    try: 
        log_file, debug = read_log_file(file_path)
        df = convert_to_dataframe(log_file)
        df = mapColumns(df)       
    except Exception as e:
        column_sk.markdown("<p class='font3'> Error while parsing log file :  </p> " + f"{type(e).__name__}: {e}",unsafe_allow_html=True)
        return None
    column_sk.markdown("<p class='font3'>Sidekiq </p> ",unsafe_allow_html=True)
    metadata = metadataSK(df)
    for x, meta_in in enumerate(metadata.keys()):
        column_sk.markdown(f"- <p class='font5' style='display:inline'>{meta_in}</p> : {str(metadata[meta_in])}", unsafe_allow_html=True)
    fig_sdk = plot_rps(df, "5 Seconds")
    del df
    return fig_sdk

def getGitalyDataFrame(column_gt,file_path):
    try: 
        log_file, debug = read_log_file_gt(file_path)
        df = convert_to_dataframe(log_file)
        df = mapColumnsGT(df)
        metadata = metadataGT(df)
    except Exception as e:
        column_gt.markdown("<p class='font3'> Error while parsing log file :  </p> " + f"{type(e).__name__}: {e}",unsafe_allow_html=True)
        return None
    column_gt.markdown("<p class='font3'>Gitaly </p> ",unsafe_allow_html=True)
    for x, meta_in in enumerate(metadata.keys()):
        column_gt.markdown(f"- <p class='font5' style='display:inline'>{meta_in}</p> : {str(metadata[meta_in])}", unsafe_allow_html=True)
    fig_gitaly = plot_rps(df, "5 Seconds")
    del df
    return fig_gitaly

if __name__ == "__main__":
    main()
