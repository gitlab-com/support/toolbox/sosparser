import json
import matplotlib.pylab as plt
import pandas as pd
from .utils import *
from pandas import json_normalize

api_columns = ['time',
 'severity',
 'duration_s',
 'status',
 'method',
 'path',
 'params',
 'host',
 'remote_ip',
 'ua',
 'route',
 'cpu_s',
 'pid',
 'worker_id',
 'rate_limiting_gates',
 'correlation_id',
 'meta.caller_id',
 'meta.remote_ip',
 'meta.feature_category',
 'meta.user',
 'meta.user_id',
 'meta.project',
 'meta.root_namespace',
 'meta.client_id',
 'meta.pipeline_id',
 'meta.job_id',
 'content_length',
 'request_urgency',
 'meta.artifact_size',
 'user_id',
 'username',
 'token_type',
 'token_id',
 'content_range',
 'meta.subscription_plan',
 'api_error',
 'net_ldap_count',
 'net_ldap_duration_s']

def read_log_file_api(file_path):
    lines =[]
    debug = []
    file_path = selectPath(file_path)
    with open(file_path, 'r') as file:
        log_lines = file.readlines()
    for x, line in enumerate(log_lines):
        try:
            l = json.loads(line)
            lines.append(l)
            if len(l) == 3 :
                if len(l["message"].split(" ")) < 4:
                    debug.append(l)
        except:
            if line != "\n":
                debug.append(line)
    return [lines, debug]

def convert_to_dataframe(log_data):
    return json_normalize(log_data)


def mapColumnsAPI(df):
    to_time=['time']
    to_numeric = ['status', 'db_count', 'cpu_s', 'duration_s',  'db_duration_s', 'view_duration_s', 'redis_duration_s', 'mem_total_bytes', 'mem_bytes']
    for l in to_time:
        df[l] = pd.to_datetime(df[l])
    for l in to_numeric:
        df[l] = pd.to_numeric(df[l])
    df.fillna(0, inplace=True)
    return df 


def metadataAPI(df):
    meta_ = {}
    meta_['Count'] = df.query('correlation_id != ""')['correlation_id'].count()
    meta_['RPS'] = round((meta_['Count']/(df.iloc[-1]['time'] - df.iloc[0]['time']).total_seconds()),1)
    meta_['Duration'] = timeConversion(df['duration_s'].sum())
    meta_['DB Duration'] = timeConversion(df['db_duration_s'].sum())
    meta_['Redis Duration'] = timeConversion( df['redis_duration_s'].sum())
    meta_['Gitaly Duration'] = timeConversion( df['gitaly_duration_s'].sum())
    meta_['CPU'] = timeConversion( df['cpu_s'].sum())
    meta_['Memory'] = convert_storage_units(df['mem_bytes'].sum()/1024,'KB')
    return meta_


dict_cType = {
    'Project':'meta.project',  'Path':'path', 'User': 'meta.user', 'Puma Worker' : 'worker_id', 'Route' : 'route'
}

dict_filter = {
    'Duration':'duration_s', 'Memory' : 'mem_bytes' ,'DB Duration':'db_duration_s', 'CPU' :'cpu_s'
}

def getTopInfoAPI(df, cType='meta.project',filter_type = 'duration_s'):
    cType = dict_cType[cType]
    filter_type = dict_filter[filter_type]
    t_duration = {}
    df = df.loc[df[cType] != 0]
    sorted_duration = df[cType].value_counts().sort_values(ascending=False)
    for value in sorted_duration.index:
        t_duration[value] = df.query("`{}` == '{}'".format(cType, value))['{}'.format(filter_type)].sum()
    t_duration = sorted(t_duration.items(), key=lambda x: x[1], reverse=True)
    cType_data = []
    for cType_ in t_duration[:10]:
        t = {}
        t['TYPE'] = cType_[0]
        counts = df.query("`{}` == '{}'".format(cType, cType_[0]))[cType].value_counts()
        t['COUNT'] = counts.iloc[0] if not counts.empty else 0
        t['DUR'] = timeConversion(df.query("`{}` == '{}'".format(cType, cType_[0]))['duration_s'].sum())
        t['DB_DUR'] = timeConversion(df.query("`{}` == '{}'".format(cType, cType_[0]))['db_duration_s'].sum())
        t['REDIS'] = timeConversion(df.query("`{}` == '{}'".format(cType, cType_[0]))['redis_duration_s'].sum())
        t['GTLY'] = timeConversion(df.query("`{}` == '{}'".format(cType, cType_[0]))['gitaly_duration_s'].sum())
        t['CPU'] = timeConversion(df.query("`{}` == '{}'".format(cType, cType_[0]))['cpu_s'].sum())
        t['MEM'] = convert_storage_units(df.query("`{}` == '{}'".format(cType, cType_[0]))['mem_bytes'].sum()/1024,"kb")
        cType_data.append(t)
    return cType_data 

def selectPath(file_path):
    if os.path.exists(file_path + "/var/log/gitlab/gitlab-rails/api_json.log"):
        return file_path + "/var/log/gitlab/gitlab-rails/api_json.log"
    else:
        return file_path + "/var/log/apps/gitlab/gitlab-rails/api_json.log"
