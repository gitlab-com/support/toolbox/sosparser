import os
import tarfile
import shutil
import json
from urllib.parse import quote
from datetime import datetime

from helpers.file_process import *

def extract_tar_files(directory):
    tar_files = [f for f in os.listdir(directory) if f.endswith('.tar.gz')]
    for tar_file in tar_files:
        base_name = os.path.splitext(os.path.splitext(tar_file)[0])[0]
        extract_path = os.path.join(directory, base_name)
        if os.path.exists(extract_path):
            shutil.rmtree(extract_path)
        os.makedirs(extract_path)
        with tarfile.open(os.path.join(directory, tar_file), 'r:gz') as tar:
            tar.extractall(path=extract_path)


def find_gitlab_status_file(folder_):
    for root, _, files in os.walk(folder_):
        if 'gitlab_status' in files:
            return os.path.join(root, 'gitlab_status'), root
    return None

def extract_services(folder_):
    services_dict = {}
    gitlab_status_path, root_path = find_gitlab_status_file(folder_)
    if gitlab_status_path:
        try:
            with open(gitlab_status_path, 'r') as f:
                for line in f:
                    if line.strip():
                        words = line.split()
                        if len(words) >= 2:
                            status = words[0].lower().rstrip(':')
                            service = words[1].lower().rstrip(':')
                            services_dict[service] = status
        except FileNotFoundError:
            print(f'gitlab_status file not found in {folder_}')
    else:
        print(f'gitlab_status file not found in {folder_}')
    return services_dict, root_path


def generate_metadata(directory):
    metadata = {
        "nodes": []
    }
    output_file = os.path.join(directory, 'metadata.json')
    
    for node_name in os.listdir(directory):
        node_path = os.path.join(directory, node_name)
        if os.path.isdir(node_path):
            services, node_path = extract_services(node_path)
            metadata["nodes"].append({
                "time_stamp": extract_timestamp(node_path),
                "node_name": host_name(node_path),
                "path": node_path,
                "services": services,
                "migration" : failed_migrations(node_path),
                "memory" : meminfo(node_path),
                "uptime" : uptime(node_path),
                "license" : parse_license_file(node_path)
            })
    with open(output_file, 'w') as f:
        json.dump(metadata, f, indent=4)

def generate_encoded_url(file_path):
    encoded_path = quote(file_path)
    return f"http://localhost:8501?path={encoded_path}"

def extract_timestamp(log_file_path):
    try:
        with open(log_file_path + "/gitlabsos.log", 'r') as file:
            lines = file.readlines()
            if lines:
                last_line = lines[-1].strip()
                timestamp = last_line.split(' ')[0].strip('[]')
                return timestamp
            else:
                return None
    except FileNotFoundError:
        print(f"File not found: {log_file_path}")
        return None

def host_name(root_path):
    try:
        with open(root_path + '/hostname', 'r') as file:
            first_line = file.readline().strip()  # Reads the first line and removes any leading/trailing whitespace
        return first_line
    except Exception as e:
        return f"An error occurred: {e}"

def render_button_link(column, text, url):
    button_html = f"""
    <a href="{url}" style="
        display: inline-block;
        padding: 8px 16px;
        font-size: 16px;
        color: white;
        background-color: #4CAF50;
        text-decoration: none;
        border-radius: 4px;">
        {text}
    </a>
    """
    column.markdown(button_html, unsafe_allow_html=True)