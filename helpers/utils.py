
import json, os
from datetime import datetime as dt
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from st_aggrid import AgGrid, GridOptionsBuilder

# global variables
error_class = "alert alert-danger"
success_class = "alert alert-success"
# checks if connected to the internal server : time out is 3 seconds
# def isInternalNetwork():
#     response =  os.system("ping -c 1 -W 3 " + "10.50.40.166")
#     return False if (response>0) else True

files_ = [
    "/lscpu",
    "/free_m",
    "/uptime",
    "/gitlab_status",
    "/gitlab_migrations",
    "/df_hT",
    "/pressure_cpu.txt",
    "/top_cpu"
]

time_frame = {
    '1 Second': '1s',
    '5 Seconds': '5s',
    '10 Seconds': '10s',
    '30 Seconds': '30s',
    '1 Minute': '1T',
    '5 Minutes': '5T',
    '10 Minutes': '10T'
}

services_dict = [
    "puma",
    "sidekiq",
    "gitaly"]

def checkFileExists(realtivePath, logsType):
    logsType_ = {
        "Production": ["/var/log/gitlab/gitlab-rails/production_json.log","/var/log/apps/gitlab/gitlab-rails/production_json.log"],
        "Sidekiq": ["/var/log/gitlab/sidekiq/current","/var/log/apps/gitlab/sidekiq"],
        "Gitaly": ["/var/log/gitlab/gitaly/current","/var/log/apps/gitlab/sidekiq"],
        "API": ["/var/log/gitlab/gitlab-rails/api_json.log","/var/log/apps/gitlab/gitlab-rails/api_json.log"],
        "Workhorse" : ["/var/log/gitlab/gitlab-workhorse/current","/var/log/apps/gitlab/gitlab-workhorse/current"],
        "Audit Events" : ["/var/log/gitlab/gitlab-rails/audit_json.log","/var/log/apps/gitlab/gitlab-rails/audit_json.log"],
        "puma": ["/var/log/gitlab/gitlab-rails/production_json.log","/var/log/apps/gitlab/gitlab-rails/production_json.log"],
        "sidekiq": ["/var/log/gitlab/sidekiq/current","/var/log/apps/gitlab/sidekiq"],
        "gitaly": ["/var/log/gitlab/gitaly/current","/var/log/apps/gitlab/sidekiq"]
    }
    if os.path.exists(realtivePath + logsType_[logsType][0]):
        return True
    elif os.path.exists(realtivePath + logsType_[logsType][1]):
        return True
    else:
        return False

def getFilteredDataSK(df):
    return df[["time", "severity", "cpu_s", "duration_s", "db_duration_s"]]


def getJobLogsForCorrelationID(correlation_id, realtivePath, logsType):
    logsType_ = {
        "Production": "/var/log/gitlab/gitlab-rails/production_json.log",
        "Sidekiq": "/var/log/gitlab/sidekiq/current",
        "Gitaly": "/var/log/gitlab/gitaly/current",
        "API": "/var/log/gitlab/gitlab-rails/api_json.log"
    }
    correlation_id_ = []
    for c in correlation_id:
        correlation_id_.append(c["correlation_id"])
    read_path = realtivePath + logsType_[logsType]
    lines = []
    debug = []
    with open(
        read_path , "r"
    ) as file:
        log_lines = file.readlines()
    for x, line in enumerate(log_lines):
        try:
            l = json.loads(line)
            if any(item in l["correlation_id"] for item in correlation_id_): 
                lines.append(l)
        except:
            pass
    return lines




def convert_storage_units(value, input_unit="KB"):
    if input_unit.upper() == "KB":
        mb = value / 1024
    elif input_unit.upper() == "MB":
        mb = value
    if mb < 1024:
        return f"{mb:.2f} MB"
    else:
        gb = mb / 1024
        if gb < 1024:
            return f"{gb:.2f} GB"
        else:
            tb = gb / 1024
            return f"{tb:.2f} TB"


custom_css_prd = {
    ".ag-row-hover": {"background-color": "#70dfdb !important"},
    ".ag-header": {
        "background-color": "white !important",
        "font-size": "15px !important",
        "font-color": "black !important",
        "border": "1px solid #eee !important;",
        "align": "left !important",
    },
}


def timeConversion(seconds):
    hours, rem = divmod(seconds, 3600)
    minutes, seconds = divmod(rem, 60)
    formatted_duration = f"{int(hours)}h" if hours >= 1 else ""
    formatted_duration += f"{int(minutes)}m" if minutes >= 1 else ""
    formatted_duration += f"{seconds:.1f}s" if seconds >= 0.1 else ""
    return formatted_duration

def saveLogEntry(logPath, comment):
    current_data = {
        "Timestamp": dt.now().strftime('%Y-%m-%d %H:%M:%S'),
        "Comment": comment,
        "Path": logPath
    }
    
    try:
        with open('log_history.json', 'r') as file:
            data = json.load(file)
    except (FileNotFoundError, json.JSONDecodeError):
        data = []
    
    data.insert(0, current_data)
    
    if len(data) > 10:
        data.pop()

    with open('log_history.json', 'w') as file:
        json.dump(data, file, indent=4)


def showLogHistory():
    try:
        with open('log_history.json', 'r') as file:
            data = json.load(file)
    except (FileNotFoundError, json.JSONDecodeError):
        data = []
    return data

def calculate_rps(df, timeframe='1s'):
    df = df.sort_values('time')   
    rps = df.set_index('time').resample(timeframe).size()
    rps_list = rps.tolist()
    time_list = rps.index.tolist()  
    return time_list, rps_list

def plot_rps(df, timeframe='5 Seconds'):
    timeframe = time_frame[timeframe]
    time_list, rps_list = calculate_rps(df, timeframe)
    # Render graph based on DPI, not pixels
    dpi = 100
    fig, ax = plt.subplots(figsize=(30, 4), dpi=dpi)
    
    ax.plot(time_list, rps_list, marker='o', linestyle='-', color='blue', label=f'RPS {timeframe}')
    total_duration = time_list[-1] - time_list[0]
    ax.set_xlabel(f'Log file duration: {total_duration}')
    ax.set_ylabel(f'Log count per {timeframe}')
    ax.set_title(f'Logs per timeframe: {timeframe}')
    ax.grid(True)
    ax.legend(loc='upper right')
    
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M:%S'))
    ax.xaxis.set_major_locator(mdates.AutoDateLocator())
    
    return fig

def setupAGChart(df, hide_columns):
    gb = GridOptionsBuilder.from_dataframe(df)

    # makes columns resizable, sortable and filterable by default
    gb.configure_default_column(
        resizable=True,
        filterable=True,
        sortable=True,
        editable=False,
        width="100%",
        theme="ag-solar",
        enable_enterprise_modules = False
    )
    gb.configure_side_bar()  # Add a sidebar
    gb.configure_selection("multiple", use_checkbox=True)  # Enable multi-row selection
    gb.configure_pagination(
        enabled=True, paginationAutoPageSize=False, paginationPageSize=20
    )
    enable_enterprise_modules = False
    if len(hide_columns) > 0:
        for col in df.columns:
            if col not in hide_columns:
                gb.configure_column(col,hide=True)
    return gb.build()


def setupSmallAGChart(df):
    gb = GridOptionsBuilder.from_dataframe(df)

def style_dataframe(df):
    return df.style.set_properties(**{
        'background-color': '#1e1e1e',
        'color': 'white',
        'border-color': '#444',
        'font-size': '14px',
        'text-align': 'left'
    }).set_table_styles([
        {'selector': 'th', 'props': [('background-color', '#2c2c2c'), ('color', 'white'), ('font-weight', 'bold')]},
        {'selector': 'td', 'props': [('border', '1px solid #444')]},
    ]).format(lambda v: v.replace(' │ ', '  |  ')) 
