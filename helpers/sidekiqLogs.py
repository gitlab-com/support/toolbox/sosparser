import os
from pandas import json_normalize
import streamlit as st
import matplotlib.pyplot as plt
import pandas as pd
import json
from .utils import time_frame

sidekiq_columns = [
    "severity",
    "time",
    "retry",
    "meta.remote_ip",
    "meta.user",
    "meta.user_id",
    "meta.related_class",
    "meta.pipeline_id",
    "external_http_count",
    "external_http_duration_s",
    "queue",
    "class",
    "jid",
    "created_at",
    "scheduled_at",
    "correlation_id",
    "meta.feature_category",
    "message",
    "job_status",
    "meta.project",
    "cpu_s",
    "gitaly_duration_s",
    "rate_limiting_gates",
    "duration_s",
    "completed_at",
    "db_duration_s"
]

def setupChartContorlsSK():
    cols = st.columns([3, 3, 3, 4, 3])
    cols[0].button("Show all columns")
    cols[1].button("Show all errors")
    cols[2].button("Show all warnings")
    cols[3].button("Get all selected rows")
    return True
    
def read_log_file(file_path):
    lines = []
    debug = []
    file_path = selectPath(file_path)
    with open(file_path, "r") as file:
        log_lines = file.readlines()
    for x, line in enumerate(log_lines):
        try:
            l = json.loads(line)
            lines.append(l)
            if len(l) == 3:
                if len(l["message"].split(" ")) < 4:
                    debug.append(l)
        except:
            if line != "\n":
                debug.append(line)
    return [lines, debug]

def selectPath(file_path):
    if os.path.exists(file_path + "/var/log/gitlab/sidekiq/current"):
        return file_path + "/var/log/gitlab/sidekiq/current"
    else:
        return file_path + "/var/log/apps/gitlab/sidekiq/current"

def convert_to_dataframe(log_data):
    return json_normalize(log_data)


dict_cType = {
    'User':'meta.user', 'Project': 'meta.project', 'Class' : 'class'
}

dict_filter = {
    'Duration':'duration_s', 'Memory' : 'mem_bytes' ,'DB Duration':'db_duration_s', 'CPU':'cpu_s'
}

def getTopInfo(df, cType='meta.user', filter_type='duration_s'):
    cType = dict_cType[cType]
    filter_type = dict_filter[filter_type]
    
    # Calculate total values for percentages
    total_count = df['job_status'].value_counts().get('done', 0)
    total_duration = df['duration_s'].sum()
    total_db_duration = df['db_duration_s'].sum()
    total_redis_duration = df['redis_duration_s'].sum()
    total_gitaly_duration = df['gitaly_duration_s'].sum()
    total_cpu_duration = df['cpu_s'].sum()
    total_memory = df['mem_bytes'].sum()
    
    df_filtered = df[df[cType] != 0]
    
    # Group by cType and calculate aggregates
    grouped = df_filtered.groupby(cType).agg({
        'job_status': lambda x: x.value_counts().get('done', 0),
        'duration_s': 'sum',
        'db_duration_s': 'sum',
        'redis_duration_s': 'sum',
        'gitaly_duration_s': 'sum',
        'cpu_s': 'sum',
        'mem_bytes': 'sum'
    }).reset_index()
    
    # Sort by duration in descending order
    grouped = grouped.sort_values('duration_s', ascending=False)
    
    cType_data = []
    for _, row in grouped.head(10).iterrows():
        t = {}
        count = row['job_status']
        duration = row['duration_s']
        db_duration = row['db_duration_s']
        redis_duration = row['redis_duration_s']
        gitaly_duration = row['gitaly_duration_s']
        cpu_duration = row['cpu_s']
        memory = row['mem_bytes']
#        queue_duration = duration - db_duration - redis_duration - gitaly_duration - cpu_duration
        t['TYPE'] = row[cType]
        t['COUNT'] = f"{count}  │  {count/total_count:.2%}"
        t['RPS'] = f"{count/duration:.2f}  |  {count/total_count:.2%}"
        t['DUR'] = f"{timeConversion(duration)}  │  {duration/total_duration:.2%}"
        t['DB'] = f"{timeConversion(db_duration)} │ {db_duration/total_db_duration:.2%}"
        t['REDIS'] = f"{timeConversion(redis_duration)}  │  {redis_duration/total_redis_duration:.2%}"
        t['GITALY'] = f"{timeConversion(gitaly_duration)}  │  {gitaly_duration/total_gitaly_duration:.2%}"
#        t['QUEUE'] = f"{timeConversion(queue_duration)} │ {queue_duration/total_duration:.2%}"
        t['CPU'] = f"{timeConversion(cpu_duration)}  │  {cpu_duration/total_cpu_duration:.2%}"
        t['MEM'] = f"{convert_storage_units(memory/1024, 'KB')}  │  {memory/total_memory:.2%}"
        t['FAIL_CT'] = f"0 │ 0.00%"
        cType_data.append(t)
    
    return cType_data

def metadataSK(df):
    meta_ = {}
    meta_['Count'] = df.query('job_status =="done"')['job_status'].count()
#    meta_['RPS'] = round((meta_['Count']/(df.iloc[-1]['time'] - df.iloc[0]['time']).total_seconds()),1)
    meta_['Duration'] = timeConversion( df['duration_s'].sum())
    meta_['DB Duration'] = timeConversion( df['db_duration_s'].sum())
    meta_['Redis Duration'] = timeConversion( df['redis_duration_s'].sum())
    meta_['Gitaly Duration'] = timeConversion( df['gitaly_duration_s'].sum())
    meta_['CPU'] = timeConversion( df['cpu_s'].sum())
    meta_['Memory'] = convert_storage_units(df['mem_bytes'].sum()/1024,"KB")
    return meta_


def timeConversion(seconds):
    hours, rem = divmod(int(seconds), 3600)
    minutes, seconds = divmod(rem, 60)
    formatted_duration = f"{hours}h" if hours > 0 else ""
    formatted_duration += f"{minutes}m" if minutes > 0 else ""
    formatted_duration += f"{seconds}s"
    return formatted_duration

def mapColumns(df):
    to_time = ["completed_at", "time", "created_at"]
    to_numeric = [
        "duration_s",
        "cpu_s",
        "redis_duration_s",
        "redis_read_bytes",
        "redis_write_bytes",
        "redis_queues_duration_s",
        "mem_objects",
        "mem_bytes",
        "mem_mallocs",
        "mem_total_bytes",
    ]
    to_string = ["correlation_id", "message", "class", "jid"]
    df = df.copy()
    
    for column in df.columns:
        if column in to_time:
            try:
                df[column] = pd.to_datetime(df[column], errors='coerce')
                df[column] = df[column].fillna(pd.NaT)  
            except:
                pass
                
        elif column in to_numeric:
            try:
                df[column] = pd.to_numeric(df[column], errors='coerce')
                df[column] = df[column].fillna(0)
            except:
                pass
                
        elif column in to_string:
            df[column] = df[column].astype('object')
            df[column] = df[column].fillna('')
            df[column] = df[column].astype(str)
            
    return df

def showWarningsSK(df):
    return df.query('severity == "WARN"')[['time', 'correlation_id' ,'severity','message']]

def showErrorsSK(df):
    return df.query('severity == "ERROR"')[['time' ,'correlation_id' , 'severity','message']]

def convert_storage_units(value, input_unit="KB"):
    if input_unit.upper() == "KB":
        mb = value / 1024
    elif input_unit.upper() == "MB":
        mb = value
    if mb < 1024:
        return f"{mb:.2f} MB"
    else:
        gb = mb / 1024
        if gb < 1024:
            return f"{gb:.2f} GB"
        else:
            tb = gb / 1024
            return f"{tb:.2f} TB"

def get_error_warn_df(df, severity):
    filtered_df = df[df['severity'] == severity]
    issue_counts = filtered_df['message'].value_counts().head(10)
    issue_summary = []
    for message, count in issue_counts.items():
        first_occurrence = filtered_df[filtered_df['message'] == message].iloc[0]
        issue_summary.append({
            'Message': message,
            'Count': count,
            'Correlation ID': first_occurrence.get('correlation_id', 'N/A'),
            'Project': first_occurrence.get('meta.project', 'N/A'),
            'Pipeline ID': first_occurrence.get('meta.pipeline_id', 'N/A'),
            'Job ID': first_occurrence.get('jid', 'N/A')
        })
    return pd.DataFrame(issue_summary)

def scheduling_latency(df):
    df['scheduling_latency_s'] = pd.to_numeric(df['scheduling_latency_s'], errors='coerce')

    latency_stats = df.groupby('class').agg({
        'scheduling_latency_s': ['count', 'min', 'max', 'mean'],
        'job_status': lambda x: (x == 'start').sum()
    }).reset_index()

    latency_stats.columns = ['Job Class', 'Times scheduled', 'Min latency', 'Max latency', 'Avg latency', 'Start count']

    # Sort by average latency in descending order
    latency_stats = latency_stats.sort_values('Avg latency', ascending=False).head(10)
    latency_stats = latency_stats.style.format({'Min latency': '{:.2f}s', 'Max latency': '{:.2f}s', 'Avg latency': '{:.2f}s'})
    return latency_stats


def latency_distribution(df):
    # Distribution of latencies
    fig, ax = plt.subplots()
    ax.hist(df['scheduling_latency_s'].dropna(), bins=50, edgecolor='black')
    ax.set_xlabel('Scheduling Latency (seconds)')
    ax.set_ylabel('Frequency')
    ax.set_title('Distribution of Scheduling Latencies')
    return fig

def job_status_pie(df):
    status_counts = df['job_status'].value_counts()
    fig, ax = plt.subplots()
    ax.pie(status_counts.values, labels=status_counts.index, autopct='%1.1f%%')
    return fig
    
def plot_comprehensive_metrics_sidekiq(df, timeframe='5 Seconds'):
    timeframe_pd = time_frame[timeframe]
    df['time'] = pd.to_datetime(df['time'],format='ISO8601', errors='coerce')
    df.set_index('time', inplace=True)
    
    # Define metrics for Sidekiq logs
    metrics = {
        'Time Metrics': ['duration_s', 'db_duration_s', 'cpu_s', 'gitaly_duration_s', 'redis_duration_s'],
        'Memory Metrics': ['mem_bytes', 'mem_total_bytes'],
        'Schedule latency': ['scheduling_latency_s', 'gitaly_duration_s', 'redis_duration_s'],
        'Job Metrics': ['jid']  
    }
    
    resampled_data = {}
    for category, columns in metrics.items():
        available_columns = [col for col in columns if col in df.columns]
        if available_columns:
            if category == 'Job Metrics':
                # Count jobs instead of summing
                resampled = df[available_columns].resample(timeframe_pd).count()
            else:
                resampled = df[available_columns].resample(timeframe_pd).sum()
            resampled_data[category] = resampled
    
    # Count errors per timeframe
    error_counts = df[df['severity'] == 'ERROR'].resample(timeframe_pd).size()
    
    df.reset_index(inplace=True)
    
    # Plotting
    num_plots = len(resampled_data) + 1  
    dpi = 100
    fig, axes = plt.subplots(num_plots, 1, figsize=(20, 4 * num_plots), dpi=dpi, sharex=True)
    
    if num_plots == 1:
        axes = [axes]  # Ensure axes is iterable
    
    plot_idx = 0

    # Plot Time Metrics
    if 'Time Metrics' in resampled_data:
        for metric in metrics['Time Metrics']:
            if metric in resampled_data['Time Metrics'].columns:
                axes[plot_idx].plot(resampled_data['Time Metrics'].index, 
                                    resampled_data['Time Metrics'][metric],
                                    marker='o', linestyle='-', label=f'{metric.replace("_", " ").title()}')
        axes[plot_idx].set_ylabel('Time (seconds)')
        axes[plot_idx].set_title('Job Duration Metrics Over Time')
        axes[plot_idx].legend(loc='upper right')
        axes[plot_idx].grid(True)
        plot_idx += 1

    if 'Schedule latency' in resampled_data:
        for metric in metrics['Schedule latency']:
            if metric in resampled_data['Schedule latency'].columns:
                axes[plot_idx].plot(resampled_data['Schedule latency'].index, 
                                    resampled_data['Schedule latency'][metric],
                                    marker='o', linestyle='-', label=f'{metric.replace("_", " ").title()}')
        axes[plot_idx].set_ylabel('Time (seconds)')
        axes[plot_idx].set_title('Schedule latency over time')
        axes[plot_idx].legend(loc='upper right')
        axes[plot_idx].grid(True)
        plot_idx += 1

    # Plot Job Counts
    if 'Job Metrics' in resampled_data:
        axes[plot_idx].plot(resampled_data['Job Metrics'].index, 
                            resampled_data['Job Metrics']['jid'],
                            marker='o', linestyle='-', label='Job Count')
        axes[plot_idx].set_ylabel('Job Count')
        axes[plot_idx].set_title('Job Counts Over Time')
        axes[plot_idx].legend(loc='upper right')
        axes[plot_idx].grid(True)
        plot_idx += 1

    # Plot Memory Metrics
    if 'Memory Metrics' in resampled_data:
        for metric in metrics['Memory Metrics']:
            if metric in resampled_data['Memory Metrics'].columns:
                axes[plot_idx].plot(resampled_data['Memory Metrics'].index, 
                                    resampled_data['Memory Metrics'][metric] / (1024 * 1024),
                                    marker='o', linestyle='-', label=f'{metric.replace("_", " ").title()} (MB)')
        axes[plot_idx].set_ylabel('Memory Usage (MB)')
        axes[plot_idx].set_title('Memory Usage Over Time')
        axes[plot_idx].legend(loc='upper right')
        axes[plot_idx].grid(True)
        plot_idx += 1
    
    # Plot Error Counts
    axes[plot_idx].plot(error_counts.index, error_counts.values,
                        marker='o', linestyle='-', color='red', label='Error Count')
    axes[plot_idx].set_ylabel('Error Count')
    axes[plot_idx].set_title('Error Counts Over Time')
    axes[plot_idx].legend(loc='upper right')
    axes[plot_idx].grid(True)

    # Set x-axis label on the last subplot
    total_duration = df.index.max() - df.index.min()
    axes[-1].set_xlabel(f'Time (Log Duration: {timeConversion(total_duration)})')
    
    plt.tight_layout()
    return plt