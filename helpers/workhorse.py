import json
import matplotlib.pylab as plt
import pandas as pd
from .utils import *
from pandas import json_normalize

wh_path =['content_type',
 'correlation_id',
 'duration_ms',
 'host',
 'level',
 'method',
 'msg',
 'proto',
 'referrer',
 'remote_addr',
 'remote_ip',
 'route',
 'status',
 'system',
 'time',
 'ttfb_ms',
 'uri',
 'user_agent',
 'written_bytes',
 'duration_s',
 'imageresizer.content_type',
 'imageresizer.original_filesize',
 'imageresizer.status',
 'imageresizer.target_width',
 'subsystem',
 'file',
 'encoding',
 'client_mode',
 'copied_bytes',
 'filename',
 'is_local',
 'is_multipart',
 'is_remote',
 'local_temp_path',
 'remote_id']

def mapColumnsWH(df):
    # Column type mappings
    to_datetime = ["time"]
    to_numeric = [
        "duration_ms", 
        "ttfb_ms", 
        "written_bytes", 
        "status"
    ]
    to_int = ["status"]
    to_bool = []
    categorical_columns = [
        "backend_id", 
        "content_type", 
        "host", 
        "level", 
        "method", 
        "msg", 
        "proto", 
        "route", 
        "route_id", 
        "system"
    ]
    df = df.copy()
    
    # Handle datetime columns
    for col in to_datetime:
        try:
            if col in df.columns:
                df[col] = pd.to_datetime(df[col], errors='coerce')
                df[col] = df[col].fillna(pd.NaT)  # Fill NA with pandas NaT
            else:
                df[col] = pd.Series(dtype='datetime64[ns]')
        except Exception as e:
            print(f"Error converting {col} to datetime: {str(e)}")
    
    # Handle numeric columns
    for col in to_numeric:
        try:
            if col in df.columns:
                df[col] = pd.to_numeric(df[col], errors='coerce')
            else:
                df[col] = pd.Series(dtype='float64')
        except Exception as e:
            print(f"Error converting {col} to numeric: {str(e)}")
    
    # Handle integer columns
    for col in to_int:
        try:
            if col in df.columns:
                df[col] = pd.to_numeric(df[col], errors='coerce').astype('Int64')
            else:
                df[col] = pd.Series(dtype='Int64')
        except Exception as e:
            print(f"Error converting {col} to integer: {str(e)}")
    
    # Handle boolean columns
    for col in to_bool:
        try:
            if col in df.columns:
                df[col] = df[col].astype('boolean')
            else:
                df[col] = pd.Series(dtype='boolean')
        except Exception as e:
            print(f"Error converting {col} to boolean: {str(e)}")

    for col in categorical_columns:
        try:
            if col in df.columns:
                df[col] = df[col].astype(str)
                df[col] = df[col].astype('category')
            else:
                df[col] = pd.Series(dtype='category')
        except Exception as e:
            print(f"Error converting {col} to category: {str(e)}")

    fill_values = {
        "duration_ms": 0,
        "ttfb_ms": 0,
        "written_bytes": 0
    }
    
    for col, value in fill_values.items():
        if col in df.columns:
            df[col] = df[col].fillna(value)
    
    return df

def read_log_file_wh(file_path):
    file_path = selectPath(file_path)
    all_logs = []
    with open(file_path, 'r') as file:
        for line in file:
            try:
                log_data = json.loads(line)
                all_logs.append(log_data)
            except json.JSONDecodeError:
                pass
    df = pd.DataFrame(all_logs)
    return df

def selectPath(file_path):
    if os.path.exists(file_path + "/var/log/gitlab/gitlab-workhorse/current"):
        return file_path + "/var/log/gitlab/gitlab-workhorse/current"
    else:
        return file_path + "/var/log/apps/gitlab/gitlab-workhorse/current"
    
def workhorse_metrics(df):
    df['time'] = pd.to_datetime(df['time'])
    time_span = (df['time'].max() - df['time'].min()).total_seconds()
    rps = len(df) / time_span if time_span > 0 else 0
    
    count_429 = (df['status'] == 429).sum()
    metrics = {
        "Count": len(df),
        "Duration": f"{timeConversion(time_span)} ",
        "RPS": f"{rps:.2f}",
        "Error Rate": f"{(df['status'] >= 400).mean() * 100:.2f}%",
        "Total Data": f"{df['written_bytes'].sum() / (1024 * 1024):.2f} MB",
        "Unique IPs": df['remote_ip'].nunique(),
        "Rate limit exceeds": count_429
    }
    
    return metrics

def workhorse_errors(df):
    errors = df[df['level'] == 'error']
    if errors.empty:
        print("No errors found in the logs.")
        return pd.DataFrame()
    error_counts = errors.groupby('error').size().reset_index(name='Count')
    error_counts = error_counts.sort_values('Count', ascending=False).head(10)

    error_summary = []
    for _, row in error_counts.iterrows():
        matching_errors = errors[errors['error'] == row['error']]
        if matching_errors.empty:
            continue
        first_occurrence = matching_errors.iloc[0]
        error_summary.append({
            'Error': row['error'],
            'Count': row['Count'],
            'Correlation ID': first_occurrence.get('correlation_id', 'N/A'),
            'URI': first_occurrence.get('uri', 'N/A'),
            'Duration (ms)': first_occurrence.get('duration_ms', 'N/A'),
            'Time': first_occurrence.get('time', 'N/A')
        })
    return pd.DataFrame(error_summary)


def workhorse_status_codes(df):
    errors = df[df['status'] >= 400]
    if errors.empty:
        return pd.DataFrame()
    status_counts = errors.groupby(['status']).size().reset_index(name='Count')
    status_counts = status_counts.sort_values('Count', ascending=False).head(10)
    
    status_summary = []
    for _, row in status_counts.iterrows():
        first_occurrence = errors[
            (errors['status'] == row['status'])
        ].iloc[0]
        
        status_summary.append({
            'Status Code': row['status'],
            'Count': row['Count'],
            'Correlation ID': first_occurrence.get('correlation_id', 'N/A'),
            'URI': first_occurrence.get('uri', 'N/A'),
            'Duration (ms)': first_occurrence.get('duration_ms', 'N/A'),
            'Time': first_occurrence.get('time', 'N/A'),
            'Error': first_occurrence.get('error', 'N/A')
        })
    
    return pd.DataFrame(status_summary)


