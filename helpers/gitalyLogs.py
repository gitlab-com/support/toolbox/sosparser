import json
import matplotlib.pylab as plt
import pandas as pd
from pandas import json_normalize

# These columns will be shown in the Gitlay logs table by default
gitaly_columns = ['correlation_id', 'grpc.code',
       'grpc.meta.client_name',  'grpc.method', 'grpc.request.deadline',
       'grpc.request.fullMethod', 'grpc.request.glProjectPath',
       'grpc.request.glRepository', 'grpc.request.payload_bytes',
       'grpc.request.repoPath', 'grpc.request.repoStorage',
       'grpc.response.payload_bytes', 'grpc.service', 'grpc.start_time',
       'grpc.time_ms', 'level', 'msg', 'peer.address', 'pid', 'remote_ip',
       'system', 'time', 'user_id', 'username', 'command.count',
       'command.cpu_time_ms', 'command.maxrss',   'response_bytes',
       'service',  'error',  'duration_ms',
       'method', 'status', 'url']

def read_log_file_gt(file_path):
    lines =[]
    debug = []
    with open(file_path + "/var/log/gitlab/gitaly/current", 'r') as file:
        log_lines = file.readlines()
    for x, line in enumerate(log_lines):
        try:
            l = json.loads(line)
            lines.append(l)
            if len(l) == 3 :
                if len(l["message"].split(" ")) < 4:
                    debug.append(l)
        except:
            if line != "\n":
                debug.append(line)
    return [lines,debug]

def convert_to_dataframe(log_data):
    return json_normalize(log_data)

def mapColumnsGT(df):
    to_time = ["grpc.start_time", "grpc.request.deadline", "time"]
    to_numeric = [
        "grpc.request.payload_bytes",
        "grpc.response.payload_bytes",
        "grpc.time_ms",
        "command.count",
        "command.cpu_time_ms",
        "command.inblock",
        "command.majflt",
        "command.maxrss",
        "command.minflt",
        "command.oublock",
        "command.real_time_ms",
        "duration_ms",
        "status"     
    ]
    df = df.copy()
    for col in to_time:
        try:
            if col in df.columns:
                df[col] = pd.to_datetime(df[col], errors='coerce')
                df[col] = df[col].fillna(pd.NaT)  
            else:
                df[col] = pd.Series(dtype='datetime64[ns]')
        except Exception as e:
            print(f"Error converting {col} to datetime: {str(e)}")
    
    # Handle numeric columns
    for col in to_numeric:
        try:
            if col not in df.columns:
                df[col] = pd.Series(dtype='float64')
            else:
                df[col] = pd.to_numeric(df[col], errors='coerce')
                if col == 'status':
                    df[col] = df[col].astype('Int64')
                else:
                    df[col] = df[col].fillna(0)
        except Exception as e:
            print(f"Error converting {col} to numeric: {str(e)}")
    
    # Group columns by prefix for specific handling
    grpc_columns = [col for col in to_numeric if col.startswith('grpc.')]
    command_columns = [col for col in to_numeric if col.startswith('command.')]
    
    # Create summary statistics (optional)
    try:
        df['total_grpc_bytes'] = df[['grpc.request.payload_bytes', 'grpc.response.payload_bytes']].sum(axis=1)
        df['total_command_time'] = df[['command.cpu_time_ms', 'command.real_time_ms']].sum(axis=1)
    except Exception as e:
        print(f"Error calculating summary statistics: {str(e)}")
    return df


def metadataGT(df):
    meta_ = {}
    meta_['Count'] = df.query('`grpc.code` == "OK"')['grpc.code'].count()
    meta_['RPS'] = round((meta_['Count']/(df.iloc[-1]['time'] - df.iloc[0]['time']).total_seconds()),1)
    meta_['Duration'] = timeConversion( df['grpc.time_ms'].sum()/1000)
    meta_['CPU'] = timeConversion( (df['command.system_time_ms'].sum()+df['command.user_time_ms'].sum())/1000)
    meta_['GIT_RSS'] = convert_storage_units(df['command.maxrss'].sum())
    meta_['Response Bytes'] =  convert_storage_units(df['response_bytes'].sum()/1024)
    meta_['Disk Read'] =  convert_storage_units(df['command.inblock'].sum())
    meta_['Disk Write'] =  convert_storage_units(df['command.oublock'].sum())
    meta_['Fail Count'] = df.query('`grpc.code` == "NotFound"')['error'].count()
    return meta_

def convert_storage_units(value, input_unit="KB"):
    if input_unit.upper() == "KB":
        mb = value / 1024
    elif input_unit.upper() == "MB":
        mb = value
    if mb < 1024:
        return f"{mb:.2f} MB"
    else:
        gb = mb / 1024
        if gb < 1024:
            return f"{gb:.2f} GB"
        else:
            tb = gb / 1024
            return f"{tb:.2f} TB"
        
def timeConversion(seconds):
    hours, rem = divmod(seconds, 3600)
    minutes, seconds = divmod(rem, 60)
    formatted_duration = f"{int(hours)}h" if hours >= 1 else ""
    formatted_duration += f"{int(minutes)}m" if minutes >= 1 else ""
    formatted_duration += f"{seconds:.1f}s" if seconds >= 0.1 else ""
    return formatted_duration

dict_cType = {
    'Project':'grpc.request.glProjectPath', 'User': 'username', 'Client':'grpc.meta.client_name', 'Service' : 'grpc.service', 'Error' :'error'
}

dict_filter = {
    'Duration':'grpc.time_ms', 'Command CPU Time' : 'command.cpu_time_ms' ,'GRPC Time (ms)':'grpc.time_ms', 'Response Bytes' : 'response_bytes'
}

time_frame = {
    '1 Second': '1s',
    '5 Seconds': '5s',
    '10 Seconds': '10s',
    '30 Seconds': '30s',
    '1 Minute': '1T',
    '5 Minutes': '5T',
    '10 Minutes': '10T'
}

metrics = {
        'Time Metrics': ['grpc.time_ms', 'duration_ms'],
        'CPU Metrics': ['command.cpu_time_ms', 'command.user_time_ms', 'command.system_time_ms'],
        'Memory Metrics': ['command.maxrss', 'command.minflt', 'command.majflt']
}
    

def getTopInfoGT(df_, cType='Project', filter_type='Duration'):
    df_['time'] = pd.to_datetime(df_['time'], format='ISO8601', errors='coerce')
    df_ = df_.dropna(subset=['time'])
    cType = dict_cType[cType]
    filter_type = dict_filter[filter_type]
    total_requests = df_.query('`grpc.code` == "OK"')['username'].count()
    t_duration = {}
    df_ = mapColumnsGT(df_)
    df = df_.loc[df_[cType] != 0]
    sorted_duration = df[cType].value_counts().sort_values(ascending=False)

    min_time = df_['time'].min()
    max_time = df_['time'].max()
    total_duration_seconds = (max_time - min_time).total_seconds()
    # Calculate total sums for percentage calculations
    total_duration = df_['grpc.time_ms'].sum() / 1000
    total_cpu_time = df_['command.cpu_time_ms'].sum() / 1000
    total_git_rss = df_['command.maxrss'].sum() / 1024
    total_resp_bytes = df_['response_bytes'].sum()
    total_disk_read = df_['command.inblock'].sum()
    total_disk_write = df_['command.oublock'].sum()
    
    for value in sorted_duration.index:
        t_duration[value] = df_.query("`{}` == '{}'".format(cType, value))['{}'.format(filter_type)].sum()
    t_duration = sorted(t_duration.items(), key=lambda x: x[1], reverse=True)
    cType_data = []

    for cType_ in t_duration[:10]:
        t = {}
        dur_ = df_.query("`{}` == '{}'".format(cType, cType_[0]))['grpc.time_ms'].sum() / 1000
        count_ = df_.query("`grpc.code` == 'OK' and `{}` == '{}'".format(cType, cType_[0]))[cType].count()
        t['TYPE'] = cType_[0]
        t['COUNT'] = f"{count_} / {get_percentage(count_, total_requests):.2f}%"
        rps = count_ / total_duration_seconds
        rps_percentage = (rps / (total_requests / total_duration_seconds)) * 100
        t['RPS'] = f"{rps:.2f} / {rps_percentage:.2f}%"
        t['DUR'] = f"{timeConversion(dur_)} / {get_percentage(dur_, total_duration):.2f}%"
        
        cpu_time = df_.query("`{}` == '{}'".format(cType, cType_[0]))['command.cpu_time_ms'].sum() / 1000
        t['CPU'] = f"{timeConversion(cpu_time)} / {get_percentage(cpu_time, total_cpu_time):.2f}%"
        
        git_rss = df_.query("`{}` == '{}'".format(cType, cType_[0]))['command.maxrss'].sum() / 1024
        t['GIT_RSS'] = f"{convert_storage_units(git_rss)} / {get_percentage(git_rss, total_git_rss):.2f}%"
        
        resp_bytes = df_.query("`{}` == '{}'".format(cType, cType_[0]))['response_bytes'].sum() 
        t['RESP_BYTES'] = f"{convert_storage_units(resp_bytes)} / {get_percentage(resp_bytes , total_resp_bytes):.2f}%"
        
        disk_read = df_.query("`{}` == '{}'".format(cType, cType_[0]))['command.inblock'].sum() 
        t['DISK_READ'] = f"{convert_storage_units(disk_read)} / {get_percentage(disk_read, total_disk_read):.2f}%"
        
        disk_write = df_.query("`{}` == '{}'".format(cType, cType_[0]))['command.oublock'].sum() 
        t['DISK_WRITE'] = f"{convert_storage_units(disk_write)} / {get_percentage(disk_write, total_disk_write):.2f}%"
        
        cType_data.append(t)
    
    return cType_data

def get_percentage(count_, total_duration):
    return (count_ / total_duration * 100) if total_duration != 0 else 0

def count_msgs_and_errors(df):
    # Count occurrences of each unique message (msg)
    msg_counts = df['msg'].value_counts().reset_index(name='count')
    msg_counts.columns = ['msg', 'count']
    
    # Count occurrences of each unique error
    error_counts = df['error'].value_counts().reset_index(name='count')
    error_counts.columns = ['error', 'count']
    
    # Filter the DataFrame to include only rows where the level is 'error'
    error_df = df[df['level'] == 'error']
    
    # Group by project path and count the total number of errors
    error_count_by_project = error_df.groupby('grpc.request.glProjectPath').size().reset_index(name='error_count')
    error_count_by_project = error_count_by_project.sort_values(by='error_count', ascending=False)
    
    # Group by username and count the total number of errors
    error_count_by_user = error_df.groupby('username').size().reset_index(name='error_count')
    error_count_by_user = error_count_by_user.sort_values(by='error_count', ascending=False)
    
    return msg_counts, error_counts, error_count_by_project, error_count_by_user


def calculate_rps(df, timeframe='1S'):
    df = df.sort_values('time')   
    rps = df.set_index('time').resample(timeframe).size()
    rps_list = rps.tolist()
    time_list = rps.index.tolist()  
    return time_list, rps_list

def plot_rps_multi(df, column_name, timeframe='5 Seconds'):
    column_name = dict_cType[column_name]
    timeframe = time_frame[timeframe]
    df_filtered = df[['time', column_name]]
    df_filtered = df_filtered[df_filtered[column_name] != 0] 
    top_entities = df_filtered[column_name].value_counts().head(5).index.tolist()
    all_time_lists = []
    all_rps_lists = []
    labels = []
    
    # iterate through each top entity and calculate the RPS
    for entity in top_entities:
        entity_df = df_filtered[df_filtered[column_name] == entity]
        time_list, rps_list = calculate_rps(entity_df, timeframe)
        all_time_lists.append(time_list)
        all_rps_lists.append(rps_list)
        labels.append(entity)
    
    # plot the results
    dpi = 100
    fig, ax = plt.subplots(figsize=(30, 4), dpi=dpi)
    
    for time_list, rps_list, label in zip(all_time_lists, all_rps_lists, labels):
        ax.plot(time_list, rps_list, marker='o', linestyle='-', label=f'{label}')
    
    total_duration = df_filtered['time'].max() - df_filtered['time'].min()
    ax.set_xlabel(f'Log file duration: {total_duration}')
    ax.set_ylabel(f'Log count per {timeframe}')
    ax.set_title(f'Logs per timeframe: {timeframe}')
    ax.grid(True)
    ax.legend(loc='upper right')
    
    return plt

def plot_comprehensive_metrics(df, timeframe='5 Seconds'):

    timeframe_pd = time_frame[timeframe]
    
    # Set 'time' as the index for resampling
    df.set_index('time', inplace=True)
    
    resampled_data = {}
    for category, columns in metrics.items():
        available_columns = [col for col in columns if col in df.columns]
        if available_columns:
            resampled = df[available_columns].resample(timeframe_pd).sum()
            resampled_data[category] = resampled
    if 'level' in df.columns:
        error_counts = df[df['level'] == 'error'].resample(timeframe_pd).size()
    else:
        error_counts = None
    
    # Reset index to get 'time' back as a column
    df.reset_index(inplace=True)
    
    # Plotting
    num_plots = len(resampled_data) + (1 if error_counts is not None else 0)
    dpi = 100
    fig, axes = plt.subplots(num_plots, 1, figsize=(25, 4 * num_plots), dpi=dpi, sharex=True)
    
    if num_plots == 1:
        axes = [axes]  # Ensure axes is iterable
    
    plot_idx = 0
    
    # Plot Time-related Metrics
    if 'Time Metrics' in resampled_data:
        axes[plot_idx].plot(resampled_data['Time Metrics'].index, resampled_data['Time Metrics']['grpc.time_ms'],
                            marker='o', linestyle='-', label='gRPC Time (ms)')
        axes[plot_idx].plot(resampled_data['Time Metrics'].index, resampled_data['Time Metrics']['duration_ms'],
                            marker='o', linestyle='-', label='Duration (ms)')
        axes[plot_idx].set_ylabel('Time (ms)')
        axes[plot_idx].set_title('Time-related Metrics Over Time')
        axes[plot_idx].legend(loc='upper right')
        axes[plot_idx].grid(True)
        plot_idx += 1
    
    # Plot CPU Metrics
    if 'CPU Metrics' in resampled_data:
        axes[plot_idx].plot(resampled_data['CPU Metrics'].index, resampled_data['CPU Metrics']['command.cpu_time_ms'],
                            marker='o', linestyle='-', label='CPU Time (ms)')
        axes[plot_idx].plot(resampled_data['CPU Metrics'].index, resampled_data['CPU Metrics']['command.user_time_ms'],
                            marker='o', linestyle='-', label='User Time (ms)')
        axes[plot_idx].plot(resampled_data['CPU Metrics'].index, resampled_data['CPU Metrics']['command.system_time_ms'],
                            marker='o', linestyle='-', label='System Time (ms)')
        axes[plot_idx].set_ylabel('CPU Time (ms)')
        axes[plot_idx].set_title('CPU Metrics Over Time')
        axes[plot_idx].legend(loc='upper right')
        axes[plot_idx].grid(True)
        plot_idx += 1
    
    # Plot Memory Metrics
    if 'Memory Metrics' in resampled_data:
        axes[plot_idx].plot(resampled_data['Memory Metrics'].index, resampled_data['Memory Metrics']['command.maxrss'],
                            marker='o', linestyle='-', label='Max RSS (KB)')
        axes[plot_idx].plot(resampled_data['Memory Metrics'].index, resampled_data['Memory Metrics']['command.minflt'],
                            marker='o', linestyle='-', label='Minor Faults')
        axes[plot_idx].plot(resampled_data['Memory Metrics'].index, resampled_data['Memory Metrics']['command.majflt'],
                            marker='o', linestyle='-', label='Major Faults')
        axes[plot_idx].set_ylabel('Memory Usage')
        axes[plot_idx].set_title('Memory Metrics Over Time')
        axes[plot_idx].legend(loc='upper right')
        axes[plot_idx].grid(True)
        plot_idx += 1
    
    # Plot Error Counts
    if error_counts is not None:
        axes[plot_idx].plot(error_counts.index, error_counts.values,
                            marker='o', linestyle='-', color='red', label='Error Count')
        axes[plot_idx].set_ylabel('Error Count')
        axes[plot_idx].set_title('Error Counts Over Time')
        axes[plot_idx].legend(loc='upper right')
        axes[plot_idx].grid(True)
    
    # Set x-axis label on the last subplot
    total_duration = df['time'].max() - df['time'].min()
    axes[-1].set_xlabel(f'Time (Log Duration: {total_duration})')
    
    plt.tight_layout()
    return plt