import json
import matplotlib.pylab as plt
from .utils import *
import pandas as pd
from pandas import json_normalize

prodcution_columns = [
    "method",
    "path",
    "format",
    "controller",
    "action",
    "status",
    "time",
    "correlation_id",
    "meta.caller_id",
    "meta.remote_ip",
    "meta.project",
    "ua",
    "remote_ip",
    "meta.user",
    "meta.user_id",
    "username",
    "ua",
    "request_urgency",
    "db_count",
    "cpu_s",
    "exception.class",
    "exception.message",
    "exception.cause_class",
    "pid",
    "worker_id",
    "gitaly_duration_s",
    "rate_limiting_gates",
    "db_duration_s",
    "duration_s",
]


def read_log_file_pr(file_path):
    lines = []
    debug = []
    file_path = selectPath(file_path)
    with open(
        file_path , "r"
    ) as file:
        log_lines = file.readlines()
    for x, line in enumerate(log_lines):
        try:
            l = json.loads(line)
            lines.append(l)
            if len(l) == 3:
                if len(l["message"].split(" ")) < 4:
                    debug.append(l)
        except:
            if line != "\n":
                debug.append(line)
    return [lines, debug]

def selectPath(file_path):
    if os.path.exists(file_path + "/var/log/gitlab/gitlab-rails/production_json.log"):
        return file_path + "/var/log/gitlab/gitlab-rails/production_json.log"
    else:
        return file_path + "/var/log/apps/gitlab/gitlab-rails/production_json.log"

dict_cType = {
    'Controller':'controller',  'Project':'meta.project','Path': 'path', 'Remote IP' : 'remote_ip', 'User': 'meta.user', 'Worker ID' : 'worker_id', 'User Agent' : 'ua', 'Exception Class' : 'exception.class', 'Errors': 'exception.message'
}

dict_filter = {
    'Duration':'duration_s', 'Memory' : 'mem_bytes' ,'DB Duration':'db_duration_s', 'CPU' :'cpu_s'
}

def getTopInfoPD(df_, cType='Project', filter_type='Duration'):
    df_['time'] = pd.to_datetime(df_['time'], format='ISO8601', errors='coerce')
    df_ = df_.dropna(subset=['time'])
    if cType == 'Controller':
        df_['controller'] = df_['controller'] + df_['action']
    df_ = mapColumnsPD(df_)
    cType = dict_cType[cType]
    filter_type = 'duration_s'
    total_requests = df_.shape[0]
    min_time = df_['time'].min()
    max_time = df_['time'].max()
    total_duration_seconds = (max_time - min_time).total_seconds()
    total_duration = df_['duration_s'].sum()

    total_db_duration = df_['db_duration_s'].sum()
    total_redis_duration = df_['redis_duration_s'].sum()
    total_gitaly_duration = df_['gitaly_duration_s'].sum()
    total_cpu_duration = df_['cpu_s'].sum()
    total_memory = df_['mem_bytes'].sum()
    
    df_ = df_.loc[df_[cType] != 0]
    
    # Group by cType and calculate aggregates
    grouped = df_.groupby(cType).agg({
        filter_type: 'sum',
        'db_duration_s': 'sum',
        'redis_duration_s': 'sum',
        'gitaly_duration_s': 'sum',
        'cpu_s': 'sum',
        'mem_bytes': 'sum'
    }).reset_index()
    
    # Add count column
    grouped['count'] = df_[cType].value_counts()[grouped[cType]].values
    
    # Sort by the filter_type in descending order
    grouped = grouped.sort_values(filter_type, ascending=False).head(10)
    
    cType_data = []
    
    for _, row in grouped.iterrows():
        t = {}
        t['TYPE'] = row[cType]
        count_ = row['count']
        t['COUNT'] = f"{count_} / {get_percentage(count_, total_requests)}%"
        rps = count_ / total_duration_seconds
        rps_percentage = (rps / (total_requests / total_duration_seconds)) * 100
        t['RPS'] = f"{rps:.2f} / {rps_percentage:.2f}%"
        duration_ = row[filter_type]
        t['DUR'] = f"{timeConversion(duration_)} / {get_percentage(duration_, total_duration)}%"
        db_duration = row['db_duration_s']
        t['DB_DUR'] = f"{timeConversion(db_duration)} / {get_percentage(db_duration, total_db_duration)}%"
        redis_duration = row['redis_duration_s']
        t['REDIS'] = f"{timeConversion(redis_duration)} / {get_percentage(redis_duration, total_redis_duration)}%"
        gitaly_duration = row['gitaly_duration_s']
        t['GTLY'] = f"{timeConversion(gitaly_duration)} / {get_percentage(gitaly_duration, total_gitaly_duration)}%"
        cpu_duration = row['cpu_s']
        t['CPU'] = f"{timeConversion(cpu_duration)} / {get_percentage(cpu_duration, total_cpu_duration)}%"
        memory_ = row['mem_bytes']
        t['MEM'] = f"{convert_storage_units(memory_/1024, 'KB')} / {get_percentage(memory_, total_memory)}%"
        cType_data.append(t)
    
    return pd.DataFrame(cType_data)

def get_percentage(count_, total_requests):
    return f"{(count_ / total_requests) * 100:.2f}"

def convert_to_dataframe(log_data):
    return json_normalize(log_data)

def mapColumnsPD(df):
    to_numeric = [
        'status', 'db_count', 'cpu_s', 'queue_duration_s', 
        'duration_s', 'db_duration_s', 'view_duration_s', 
        'redis_duration_s', 'mem_total_bytes', 'mem_bytes', 'cpu_s'
    ]
    to_time = ['time']
    exception_columns = ['exception.class', 'exception.message']
    df = df.copy()
    for col in to_time:
        try:
            if col in df.columns:
                df[col] = pd.to_datetime(df[col], errors='coerce')
                df[col] = df[col].fillna(pd.NaT)
            else:
                df[col] = pd.Series(dtype='datetime64[ns]')
        except Exception as e:
            print(f"Error converting {col} to datetime: {str(e)}")

    for col in to_numeric:
        try:
            if col not in df.columns:
                df[col] = pd.Series(dtype='float64')
            else:
                df[col] = pd.to_numeric(df[col], errors='coerce')
                df[col] = df[col].fillna(0)
        except Exception as e:
            print(f"Error converting {col} to numeric: {str(e)}")

    for col in exception_columns:
        try:
            if col not in df.columns:
                df[col] = pd.Series(dtype='object')
            else:
                df[col] = df[col].astype('object')
                df[col] = df[col].fillna('')  
                df[col] = df[col].astype(str)
        except Exception as e:
            print(f"Error converting {col} to string: {str(e)}")
    
    return df

def timeConversion(seconds):
    hours, rem = divmod(seconds, 3600)
    minutes, seconds = divmod(rem, 60)
    formatted_duration = f"{int(hours)}h" if hours >= 1 else ""
    formatted_duration += f"{int(minutes)}m" if minutes >= 1 else ""
    formatted_duration += f"{seconds:.1f}s" if seconds >= 0.1 else ""
    return formatted_duration

# calculates RPS by deviding total duration with total number of requests.
def calculate_rps(df):
    total_count = len(df)
    if not df['time'].empty:
        min_time = df['time'].min()
        max_time = df['time'].max()
        total_duration_seconds = (max_time - min_time).total_seconds()
        if total_duration_seconds > 0:
            rps = total_count / total_duration_seconds
        else:
            rps = 0
    else:
        rps = 0
    return rps

#MetaData extraction

def metadataPD(df):
    total_requests = len(df)
    duration_sum = df['duration_s'].sum()
    db_duration_sum = df['db_duration_s'].sum()
    redis_duration_sum = df['redis_duration_s'].sum()
    view_duration_sum = df['view_duration_s'].sum()
    cpu_sum = df['cpu_s'].sum()
    mem_bytes_sum = df['mem_bytes'].sum()
    average_cpu = df['cpu_s'].mean()
    average_memory_mb = df['mem_bytes'].mean() / (1024 ** 2)

    meta_ = {
        'Count': total_requests,
        'Duration': timeConversion(duration_sum),
        'RPS': round(calculate_rps(df), 2) if duration_sum > 0 else 0,
        'DB Duration': timeConversion(db_duration_sum),
        'Redis Duration': timeConversion(redis_duration_sum),
        'View Duration': timeConversion(view_duration_sum),
        'CPU': timeConversion(cpu_sum),
        'Memory': f"{mem_bytes_sum / (1024 ** 3):.2f} GB",  # Convert bytes to GB
    }
    return meta_

def get_status_codes(df):
    top_status_codes = df['status'].value_counts().reset_index(name='count')
    top_status_codes.columns = ['status', 'count']
    top_status_codes = top_status_codes[top_status_codes['status'] > 0]
    total_requests = top_status_codes['count'].sum()
    top_status_codes['percentage'] = (top_status_codes['count'] / total_requests) * 100
    return top_status_codes

def plot_status_codes(df):
    plt.figure(figsize=(10, 6))
    bars = plt.bar(df['status'].astype(str), df['count'], color='skyblue')
    plt.xlabel('Status Code')
    plt.ylabel('Count')
    plt.title('Distribution of Status Codes')
    
    # Adding count and percentage labels on top of the bars
    for bar, count, percentage in zip(bars, df['count'], df['percentage']):
        yval = bar.get_height()
        plt.text(bar.get_x() + bar.get_width()/2, yval + 0.5, f'{count} ({percentage:.2f}%)', ha='center', va='bottom')
 
    return plt

def get_slowest_requests(df):
    slow_requests = df.sort_values(by='duration_s', ascending=False)
    get_columns = ['meta.project', 'username','correlation_id', 'controller', 
                       'duration_s', 'status', 'exception.class', 'exception.message']
    available_columns = [col for col in get_columns if col in df.columns]
    slow_requests = slow_requests[available_columns].head(10)
    return slow_requests

def get_request_volume_over_time(df, duration='5T'):
    duration = time_frame[duration]
    df['time'] = pd.to_datetime(df['time'])  # Ensure the 'time' column is in datetime format
    volume_over_time = df.set_index('time').resample(duration).size().reset_index(name='count')
    return volume_over_time

def plot_rps_multi_pd(df, column_name, timeframe='5 Seconds'):
    column_name = dict_cType[column_name]
    timeframe = time_frame[timeframe]
    df_filtered = df[['time', column_name]]
    df_filtered = df_filtered[df_filtered[column_name] != 0] 
    top_entities = df_filtered[column_name].value_counts().head(5).index.tolist()
    all_time_lists = []
    all_rps_lists = []
    labels = []
    
    for entity in top_entities:
        entity_df = df_filtered[df_filtered[column_name] == entity]
        time_list, rps_list = calculate_rps_pd(entity_df, timeframe)
        all_time_lists.append(time_list)
        all_rps_lists.append(rps_list)
        labels.append(entity)
    
    # plot the results
    dpi = 100
    fig, ax = plt.subplots(figsize=(30, 4), dpi=dpi)
    
    for time_list, rps_list, label in zip(all_time_lists, all_rps_lists, labels):
        ax.plot(time_list, rps_list, marker='o', linestyle='-', label=f'{label}')
    
    total_duration = df_filtered['time'].max() - df_filtered['time'].min()
    ax.set_xlabel(f'Log file duration: {total_duration}')
    ax.set_ylabel(f'Log count per {timeframe}')
    ax.set_title(f'Logs per timeframe: {timeframe}')
    ax.grid(True)
    ax.legend(loc='upper right')
    
    return plt

def calculate_rps_pd(df, timeframe='1s'):
    df = df.sort_values('time')   
    rps = df.set_index('time').resample(timeframe).size()
    rps_list = rps.tolist()
    time_list = rps.index.tolist()  
    return time_list, rps_list

def plot_comprehensive_metrics_pd(df, timeframe='5 Seconds'):

    timeframe_pd = time_frame[timeframe]
    
    # Set 'time' as the index for resampling
    df.set_index('time', inplace=True)
    
    resampled_data = {}
    for category, columns in metrics.items():
        available_columns = [col for col in columns if col in df.columns]
        if available_columns:
            resampled = df[available_columns].resample(timeframe_pd).sum()
            resampled_data[category] = resampled
#   Should I convert duration_s ms? 
#    resampled_data['Time Metrics']['duration_s']  = resampled_data['Time Metrics']['duration_s'] / 10
    # Error count per timeframe 
    if 'status' in df.columns:
        error_counts = df[df['status'] > 400].resample(timeframe_pd).size()
    else:
        error_counts = None
    df.reset_index(inplace=True)
    
    # Plotting
    num_plots = len(resampled_data) + (1 if error_counts is not None else 0)
    dpi = 100
    fig, axes = plt.subplots(num_plots, 1, figsize=(25, 4 * num_plots), dpi=dpi, sharex=True)
    
    if num_plots == 1:
        axes = [axes]
    
    plot_idx = 0

    if 'Time Metrics' in resampled_data:
        axes[plot_idx].plot(resampled_data['Time Metrics'].index, resampled_data['Time Metrics']['duration_s'],
                            marker='o', linestyle='-', label='Duration (seconds)')
        axes[plot_idx].plot(resampled_data['Time Metrics'].index, resampled_data['Time Metrics']['db_duration_s'],
                            marker='o', linestyle='-', label='DB Duration (ms)')
        axes[plot_idx].plot(resampled_data['Time Metrics'].index, resampled_data['Time Metrics']['cpu_s'],
                            marker='o', linestyle='-', label='CPU Duration (ms)')
        axes[plot_idx].plot(resampled_data['Time Metrics'].index, resampled_data['Time Metrics']['gitaly_duration_s'],
                            marker='o', linestyle='-', label='Gitaly Duration (ms)')
        axes[plot_idx].plot(resampled_data['Time Metrics'].index, resampled_data['Time Metrics']['redis_duration_s'],
                            marker='o', linestyle='-', label='Redis Duration (ms)')
        axes[plot_idx].set_ylabel('Time (ms)')
        axes[plot_idx].set_title('Response metrics over time')
        axes[plot_idx].legend(loc='upper right')
        axes[plot_idx].grid(True)
        plot_idx += 1
    
    # Plot Memory Metrics
    if 'Memory Metrics' in resampled_data:
        axes[plot_idx].plot(resampled_data['Memory Metrics'].index, resampled_data['Memory Metrics']['mem_bytes']/ (1024 * 1024),
                            marker='o', linestyle='-', label='Memory (MB)')
        axes[plot_idx].plot(resampled_data['Memory Metrics'].index, resampled_data['Memory Metrics']['mem_total_bytes']/ (1024 * 1024),
                            marker='o', linestyle='-', label='Total Memory (MB)')
        axes[plot_idx].set_ylabel('Memory Usage')
        axes[plot_idx].set_title('Memory Usage over time')
        axes[plot_idx].legend(loc='upper right')
        axes[plot_idx].grid(True)
        plot_idx += 1
    
    # Plot Error Counts
    if error_counts is not None:
        axes[plot_idx].plot(error_counts.index, error_counts.values,
                            marker='o', linestyle='-', color='red', label='Error Count')
        axes[plot_idx].set_ylabel('Error Count')
        axes[plot_idx].set_title('Error Counts Over Time')
        axes[plot_idx].legend(loc='upper right')
        axes[plot_idx].grid(True)

    # Set x-axis label on the last subplot
    total_duration = df['time'].max() - df['time'].min()
    axes[-1].set_xlabel(f'Time (Log Duration: {total_duration})')
    
    plt.tight_layout()
    return plt

metrics = {
        'Time Metrics': ['duration_s','db_duration_s','queue_duration_s', 'cpu_s', 'redis_duration_s','gitaly_duration_s'],
        'Memory Metrics': ['mem_bytes','mem_total_bytes'],
        'Error metrics': ['exception.class', 'exception.message']
}

def summarize_errors(df):
    errors = df[df['status'] > 399]
    error_counts = errors.groupby(['status', 'exception.class', 'exception.message']).size().reset_index(name='Count')
    error_counts = error_counts.sort_values('Count', ascending=False).head(10)
    
    error_summary = []
    for _, row in error_counts.iterrows():
        first_occurrence = errors[
            (errors['status'] == row['status']) & 
            (errors['exception.class'] == row['exception.class']) & 
            (errors['exception.message'] == row['exception.message'])
        ].iloc[0]
        
        error_summary.append({
            'Status': row['status'],
            'Exception Class': row['exception.class'],
            'Message': row['exception.message'],
            'Count': row['Count'],
            'Correlation ID': first_occurrence.get('correlation_id', 'N/A'),
            'Path': first_occurrence.get('path', 'N/A'),
        })
    
    return pd.DataFrame(error_summary)