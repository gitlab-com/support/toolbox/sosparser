# AgGrid (free) https://www.ag-grid.com/javascript-data-grid/getting-started/
from st_aggrid import AgGrid, DataReturnMode, GridUpdateMode

# pandas: https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html 
import pandas as pd

# streamlit : https://discuss.streamlit.io
import streamlit as st
from streamlit_option_menu import option_menu

from helpers.file_process import *
from helpers.gitalyLogs import *
from helpers.productionLogs import *
from helpers.sidekiqLogs import *
from helpers.apiJson import *
from helpers.utils import *
from helpers.workhorse import *
from helpers.auditEvents import *

from PIL import Image
from os.path import exists

def validateFilepath(file_, c):
    file_path = file_
    with c:
        for f in files_:
            if exists(file_ + f):
                st.markdown(" Read file : _*" + f + "*_  :white_check_mark:")
            else:
                return False
    return True

def main():
    initialize()
    file_path = ""
    with st.sidebar:
        # Ensure "Metadata" remains top of menu after "Home" in sidebar for quick system details check
        menuItems = ["Home", "Metadata","Version Manifest", "Gitaly", "Production", "Sidekiq","API Json", "Workhorse", "Audit Events"]
        choice = option_menu(
            "Menu",
            menuItems,
            icons=[
                "house",
                "book fill",
                "123",
                "git",
                "gear",
                "kanban",
                "arrow-down-up",
                "pc-display-horizontal",
                "file-ruled"
                
            ],
            menu_icon="app-indicator",
            default_index=0,
            styles={
                "container": {"padding": "5!important", "background-color": "#0a0a0a"},
                "icon": {"color": "orange", "font-size": "25px"},
                "nav-link": {
                    "font-size": "20px",
                    "text-align": "left",
                    "margin": "0px",
                    "--hover-color": "grey",
                },
                "nav-link-selected": {"background-color": "#02ab21"},
            },
        )

    if choice == "Home":
        logo(" SOS Parser \U0001F5C3")
        indexPage()
    elif choice == "Version Manifest":
        logo("Version Manifest :1234:")
        versionMainfestPage()
    elif choice == "Gitaly":
        logo("Gitaly Logs :hourglass_flowing_sand:")
        if checkFileExists(st.session_state.file_path,"Gitaly"):
            gitalyPage()
        else:
            logFileNotFound("Gitaly")
    elif choice == "Metadata":
        logo("Metadata :warning:")
        metadataPage()
    elif choice == "Production":
        logo("Production Logs :gear:")
        if checkFileExists(st.session_state.file_path,"Production"):
            productionLogsPage()
        else:
            logFileNotFound("Production")
    elif choice == "Sidekiq":
        logo("Sidekiq Logs \U0001F916")
        if checkFileExists(st.session_state.file_path,"Sidekiq"):
            sidekiqPage()
        else:
            logFileNotFound("Sidekiq")
    elif choice == "API Json":
        logo("API Json :spiral_note_pad:")
        if checkFileExists(st.session_state.file_path,"API"):
            apiJsonLogs()
        else:
            logFileNotFound("API")
    elif choice == "Workhorse":
        logo("Workhorse Logs :mechanical_arm:")
        if checkFileExists(st.session_state.file_path,"Workhorse"):
            workhorsePage()
        else:
            logFileNotFound("Workhorse")
    elif choice == "Audit Events":
        logo("Audit Events :ledger:")
        if checkFileExists(st.session_state.file_path,"Audit Events"):
            auditEvents()
        else:
            logFileNotFound("Audit Events")
    return True

def gitalyPage():
    if st.session_state.valid:
        df,debug = getGitalyDataFrame(st.session_state.file_path)
        df = mapColumnsGT(df)

        # Show metadata at the top of the page for Gitaly log file
        cm = st.columns([1, 1, 1, 1, 1, 1, 1, 1,1])
        metadata = metadataGT(df)
        for x, meta_in in enumerate(metadata.keys()):
            cm[x].metric(":green["+meta_in+"]", metadata[meta_in])

        # Render graph with requests on Y-axis and time on X-axis with variable duration
        option = st.selectbox(" ",  ['1 Second','5 Seconds','10 Seconds','30 Seconds', '1 Minute', '5 Minutes', '10 Minutes'],index=1, label_visibility = "collapsed")
        if option != None:              
            fig = plot_rps(df, option)
            st.pyplot(fig)
        st.divider()
        #   Setup the AGChart with custom colums and selectable rows.  The user will still be able to add more columns if needed, but default only columns defined in the ̦ will be shown in the table  
        #   The rendered table will be searchable, sortable and filterable with any column value
        st.subheader(":orange[Gitaly logs table]")
        goShort = setupAGChart(df, gitaly_columns)
        response = AgGrid(
            df, gridOptions=goShort, custom_css=custom_css_prd, allow_unsafe_jscode=True
        )

        #   Handling the selected rows, currently we new table to show selected rows and then allow user to view raw json
        selected = response["selected_rows"]
        if selected:
            st.markdown(
                '<p class="font1">  :orange[Selected rows :]  </p>', unsafe_allow_html=True
            )
            AgGrid(convert_to_dataframe(selected))
            sjl = st.button("Show Job Logs")
            if sjl:
                st.markdown(
                    '<p class="font1">  JSON Logs :  </p>', unsafe_allow_html=True
                )
                st.write(getJobLogsForCorrelationID(selected,st.session_state.file_path ,"Gitaly"))
        
        # Display top ten values of inputs (Project, Path, etc) sorted by duration in deccending order 
        top_type = st.selectbox(" ", ("Project", "User", "Client", "Service"),label_visibility = "collapsed")
        dt = pd.json_normalize(getTopInfoGT(df, top_type))
        st.subheader(":orange[Top 10 {} by duration]".format(top_type))
        display_custom_table(dt)
        st.divider()
        st.subheader(":orange[Error breakdown]")        
        error_titles= ["Unique messge count", "Unique error count", "Error count by project",  "Error count by user"]
        error_data_ = count_msgs_and_errors(df)
        error_columns = st.columns(len(error_data_))
        for x, errors_ in enumerate(error_data_):
            error_columns[x].markdown(':green[{}]'.format(error_titles[x]), unsafe_allow_html=True)
            error_columns[x].table(errors_.head(10))
        st.divider()
        col_type, col_duration, col_empty= st.columns([3,3,4])
        top_type = col_type.selectbox(" ", ["Project", "User", "Error", "Service"], label_visibility = "collapsed",key="graph_top")
        duration_ = col_duration.selectbox(" ",  ['1 Second','5 Seconds','10 Seconds','30 Seconds', '1 Minute', '5 Minutes', '10 Minutes'],index=1,label_visibility = "collapsed",key="graph_multi")
        plt_fig = plot_rps_multi(df,top_type,duration_)
        st.pyplot(plt_fig)
        st.divider()
        plt_fig = plot_comprehensive_metrics(df) 
        st.pyplot(plt_fig)
        return True
    else:
        filePathExists()

def metadataPage():
    if st.session_state.valid:
        pm1, pm2 = st.columns([6, 2])
        with pm1:
            m1, m2, m3 = st.columns([3, 2, 3], gap="small")
            m5, m6 = st.columns([2, 1], gap="small")
            # CPU info
            with m1:
                m1.markdown(':orange[CPU Details]', unsafe_allow_html=True)
                if st.session_state.valid:
                    cpu_ = extract_cpuInfo(st.session_state.file_path)
                    for c in cpu_:
                        m1.markdown(":red[" + c + "] : :green[" + str(cpu_[c]) + "]")
                    m1.markdown("---")
                else:
                    m1.markdown("No data found")
            # Memory info
            with m2:
                m2.markdown(':orange[Memory]', unsafe_allow_html=True)
                if st.session_state.valid:
                    mem_info = meminfo(st.session_state.file_path)
                    for c in mem_info:
                        m2.markdown(":red[" +c + "] : :green[" + convert_storage_units(int(mem_info[c]),"MB") + "]")
                    m2.markdown("---")
                else:
                    m2.markdown("No data found")

            # Up time
            with m3:
                m3.markdown(':orange[Uptime]  </p>', unsafe_allow_html=True)
                if st.session_state.valid:
                    up_time = uptime(st.session_state.file_path)
                    for c in up_time:
                        m3.markdown(":red[" + c + "] : :green["  + str(up_time[c]) + "]")
                    m3.markdown("---")
                else:
                    m3.markdown("No data found")

            # Top five mounts
            with m5:
                m5.markdown(
                    ':orange[Top 5 Disk mounts]', unsafe_allow_html=True
                )
                if st.session_state.valid:
                    disk_mount = parse_df_hT_output(st.session_state.file_path)
                    data_ = []
                    data_.append(disk_mount[0].keys())
                    for d in disk_mount:
                        data_.append(d.values())
                    m5.table(data_)
                    m5.markdown("---")
                else:
                    m5.markdown("No data found")

            # CPU_Test
            with m6:
                m6.markdown(
                    ':orange[CPU Pressure test]', unsafe_allow_html=True
                )
                if st.session_state.valid:
                    cpu_pressure = pressure_results(st.session_state.file_path)[0]
                    data_ = []
                    data_x = []
                    data_.append(cpu_pressure["some"].keys())
                    for d in cpu_pressure["some"]:
                        data_x.append(cpu_pressure["some"][d])
                    data_.append(data_x)
                    data_x = []
                    data_.append(cpu_pressure["full"].keys())
                    for d in cpu_pressure["full"]:
                        data_x.append(cpu_pressure["full"][d])
                    data_.append(data_x)
                    m6.table(data_)
                    m6.markdown("---")
                else:
                    m6.markdown("No data found")

            # Top 10 processes
            with pm1:
                pm1.markdown(
                    ':orange[Top 10 processes]', unsafe_allow_html=True
                )
                if st.session_state.valid:
                    top_p = extract_top_processes(st.session_state.file_path)
                    data_ = []
                    for d in top_p:
                        pm1.text(d + "\n")
                    pm1.markdown("---")
                else:
                    pm1.markdown("No data found")

            # Failed migrations
            with pm1:
                pm1.markdown(
                    ':orange[Failed migrations]', unsafe_allow_html=True
                )
                if st.session_state.valid:
                    failed_migration = failed_migrations(st.session_state.file_path)
                    data_ = []
                    if failed_migration:
                        data_.append(failed_migration[0].keys())
                    for d in failed_migration:
                        data_.append(d.values())
                    pm1.table(data_)
                    pm1.markdown("---")
                else:
                    pm1.markdown("No data found")

        # Gitlab services
        with pm2:
            pm2.markdown(':orange[GitLab Services]', unsafe_allow_html=True)
            if st.session_state.valid:
                s_status = extract_services(st.session_state.file_path)
                for s in s_status:
                    pm2.markdown(
                        "**" + s + " : " + ("✅" if s_status[s] == "run:" else "🔥") + "**"
                    )
                pm2.markdown("---")
            else:
                pm2.markdown("No data found")

            # Memory Test
            pm2.markdown(
                ':orange[Memory Pressure test]', unsafe_allow_html=True
            )
            if st.session_state.valid:
                cpu_pressure = pressure_results(st.session_state.file_path)[1]
                data_ = []
                data_x = []
                data_.append(cpu_pressure["some"].keys())
                for d in cpu_pressure["some"]:
                    data_x.append(cpu_pressure["some"][d])
                data_.append(data_x)
                data_x = []
                data_.append(cpu_pressure["full"].keys())
                for d in cpu_pressure["full"]:
                    data_x.append(cpu_pressure["full"][d])
                data_.append(data_x)
                pm2.markdown("---")
            else:
                pm2.markdown("No data found")
        return True
    else:
        filePathExists()

def productionLogsPage():
    if st.session_state.valid:
        df, debug = getProductionLogDF(st.session_state.file_path)
        df = mapColumnsPD(df)

        # Shows metadata at the top of the page for the given log file
        metadata = metadataPD(df)
        cm = st.columns([1, 1, 1, 1, 1, 1,1,1])
        for x, meta_in in enumerate(metadata.keys()):
            cm[x].metric(":green["+meta_in+"]", metadata[meta_in])

        #This block displays the log entries in line graph, the duration can be changed
        st.markdown(':red[Select timeframe :]', unsafe_allow_html=True)
        option = st.selectbox(" ", ['1 Second','5 Seconds','10 Seconds','30 Seconds', '1 Minute', '5 Minutes', '10 Minutes'],index=1,label_visibility = "collapsed", key = "PD_TF")
        if option != None:              
            fig = plot_rps(df, option)
            st.pyplot(fig)
        st.divider()
        st.subheader(":orange[Production logs table]")
        #   Setup the AGChart with custom colums and selectable rows.  The user will still be able to add more columns if needed, but default only columns defined in the ̦ will be shown in the table  
        go = setupAGChart(df, prodcution_columns)
        response = AgGrid(
            df, gridOptions=go, custom_css=custom_css_prd, allow_unsafe_jscode=True,  key = "logTablePD"
        )

        #   Handling the selected rows, currently we new table to show selected rows and then allow user to view raw json
        selected = response["selected_rows"]
        if selected:
            st.markdown(
                '<p class="font1">  Selected rows :  </p>', unsafe_allow_html=True
            )
            AgGrid(convert_to_dataframe(selected), gridOptions=go)
            sjl = st.button("Show Job Logs")
            if sjl:
                st.markdown(
                    '<p class="font1">  JSON Logs :  </p>', unsafe_allow_html=True
                )
                st.write(getJobLogsForCorrelationID(selected,st.session_state.file_path ,"Production"))
        st.divider()
        # Display top ten values of inputs (Project, Path, etc) sorted by duration in deccending order 
        top_type = st.selectbox(" ", ['Controller','Project','Path', 'Remote IP', 'User', 'Worker ID', 'User Agent'],label_visibility = "collapsed", key = "PD_TOP")
        dt = getTopInfoPD(df, top_type)
        st.subheader(":orange[Top 10 {} by duration]".format(top_type))
        display_custom_table(dt)
        st.divider()
        st.subheader(":orange[Error Breakdown]")
        col1, col2 = st.columns([7.5,2.5])

        with col1:
            st.write(":green[Top 10 Errors (400-499)]")
            client_errors = summarize_errors(df)
            st.dataframe(client_errors, height=400)

        with col2:
            #   Displays status code using chart 
            st.markdown(':green[Status code breakdown :]', unsafe_allow_html=True)
            status_count =  get_status_codes(df)
            plt = plot_status_codes(status_count)
            st.pyplot(plt)
        st.divider()

        st.subheader(":orange[Request breakdown]")
        #    Display top ten slowest procssed request (by duration)
        slow_requests, request_frequnecy =st.columns([7,3])
        slow_requests.markdown(':green[Top slow requests :]', unsafe_allow_html=True)
        slow_requests.table(get_slowest_requests(df))

        #   Breakdown of requst volume by variable time duration using chart
        request_frequnecy.markdown(':green[Request Volume :]', unsafe_allow_html=True)
        filter_type = request_frequnecy.selectbox(" ", ['1 Second','5 Seconds','10 Seconds','30 Seconds', '1 Minute', '5 Minutes', '10 Minutes'],index=1,label_visibility = "collapsed",key = "PD_DUR")
        volume_over_time = get_request_volume_over_time(df,filter_type)
        request_frequnecy.bar_chart(volume_over_time.set_index('time')['count'])
        st.divider()

        col_type, col_duration, col_empty= st.columns([3,3,4])
        top_type = col_type.selectbox(" ", ['Controller','Project','Path', 'Remote IP', 'User', 'Exception Class', 'Errors'], label_visibility = "collapsed",key="graph_top")
        duration_ = col_duration.selectbox(" ",  ['1 Second','5 Seconds','10 Seconds','30 Seconds', '1 Minute', '5 Minutes', '10 Minutes'],index=1,label_visibility = "collapsed",key="graph_multi")
        plt_fig = plot_rps_multi_pd(df,top_type,duration_)
        st.pyplot(plt_fig)
        st.divider()
        plt_fig = plot_comprehensive_metrics_pd(df) 
        st.pyplot(plt_fig)

        return True
    else:
        filePathExists()

@st.cache_data()
def getProductionLogDF(file_path):
    lines, debug = read_log_file_pr(file_path)
    df = convert_to_dataframe(lines)
    return [df,debug]

@st.cache_data()
def getSKDataFrame(file_path):
    log_file, debug = read_log_file(file_path)
    df = convert_to_dataframe(log_file)
    return [df,debug]

@st.cache_data()
def getGitalyDataFrame(file_path):
    log_file, debug = read_log_file_gt(file_path)
    df = convert_to_dataframe(log_file)
    return [df,debug]

@st.cache_data()
def getAPIDataFrame(file_path):
    log_file, debug = read_log_file_api(file_path)
    df = convert_to_dataframe(log_file)
    return [df,debug]

@st.cache_data()
def getWorkhorseLogs(file_path):
    df_logs = read_log_file_wh(file_path)
    return df_logs

@st.cache_data()
def getAuditEvents(file_path):
    df_logs, df_errors = read_log_file_ae(file_path)
    return [df_logs, df_errors]

def sidekiqPage():
    if st.session_state.valid:
        df, debug = getSKDataFrame(st.session_state.file_path)
        df = mapColumns(df)

        # Show metadata at the top of the page for Sidekiq log file
        cm = st.columns([1, 1, 1, 1, 1, 1, 1,0.5, 1, 1])
        metadata = metadataSK(df)
        for x, meta_in in enumerate(metadata.keys()):
           cm[x].metric(":green["+meta_in+"]", metadata[meta_in])
        cm[8].metric(
            ":green[Errors]", df.query('severity == "ERROR"')["severity"].value_counts().iloc[0] if not df.query('severity == "ERROR"').empty else 0
        )
        cm[9].metric(
            ":green[Warnings]", df.query('severity == "WARN"')["severity"].value_counts().iloc[0] if not df.query('severity == "WARN"').empty else 0
        )
        # Display graph with LPS
        st.markdown(':red[Select timeframe :]', unsafe_allow_html=True)
        option = st.selectbox(" ", ['1 Second','5 Seconds','10 Seconds','30 Seconds', '1 Minute', '5 Minutes', '10 Minutes'],index=1,label_visibility = "collapsed")
        if option != None:              
            fig = plot_rps(df, option)
            st.pyplot(fig)
        st.divider()
        st.subheader(":orange[Sidekiq logs table]")
        go = setupAGChart(df, sidekiq_columns)
        response = AgGrid(
            df, gridOptions=go, custom_css=custom_css_prd, allow_unsafe_jscode=True
        )
        selected = response["selected_rows"]
        if selected:
            st.markdown(
                '<p class="font1">  Selected rows :  </p>', unsafe_allow_html=True
            )
            AgGrid(convert_to_dataframe(selected))
            sjl = st.button("Show Job Logs")
            if sjl:
                st.markdown(
                    '<p class="font1">  JSON Logs :  </p>', unsafe_allow_html=True
                )
                st.write(getJobLogsForCorrelationID(selected,st.session_state.file_path ,"Sidekiq"))
        st.divider()
        slt1, slt2, slt3 = st.columns([2, 2, 6])
        top_type = slt1.selectbox(" ", ("User", "Project", "Class"))
        top_filter = slt2.selectbox(" ", ("Duration", "Memory", "DB Duration", "CPU"))
        dt = pd.json_normalize(getTopInfo(df, top_type, top_filter))
        st.subheader(":orange[Top 10 {} by duration]".format(top_type))
        display_custom_table(dt)
        st.divider()
        st.subheader(":orange[Errors and warnings]")
        col1, col2 = st.columns([5,5])
        with col1:
            st.write(":green[Top 10 Errors]")
            error_summary = get_error_warn_df(df, 'ERROR')
            st.dataframe(error_summary, height=400)
        
        with col2:
            st.write(":green[Top 10 Warnings]")
            warning_summary = get_error_warn_df(df, 'WARN')
            st.dataframe(warning_summary, height=400)
        st.divider()
        st.subheader(":orange[Scheduling latency overtime]")
        col1, col2 = st.columns([7,3])
        with col1:
            latency_stats = scheduling_latency(df)
            st.table(latency_stats)
        with col2:
            latency_dist = latency_distribution(df)
            st.pyplot(latency_dist)
        st.divider()
        st.subheader(":orange[Metrics Over Time]")
        
        timeframe = st.selectbox("Select Time Frame", ["1 Second", "5 Seconds", "30 Seconds", "1 Minute", "5 Minutes"],index=1)
        fig = plot_comprehensive_metrics_sidekiq(df, timeframe)
        st.pyplot(fig)
    else:
        filePathExists()

def apiJsonLogs():
    if st.session_state.valid:
        df, debug = getAPIDataFrame(st.session_state.file_path)
        df = mapColumnsAPI(df)
        cm = st.columns([1, 1, 1, 1, 1, 1, 1, 1])
        metadata = metadataAPI(df)
        for x, meta_in in enumerate(metadata.keys()):
            cm[x].metric(":green["+meta_in+"]", metadata[meta_in])
        # Display graph with LPS
        st.markdown('<p class="font1">  Select timeframe :</p>', unsafe_allow_html=True)
        option = st.selectbox(" ", ['1 Second','5 Seconds','10 Seconds','30 Seconds', '1 Minute', '5 Minutes', '10 Minutes'],label_visibility = "collapsed")
        if option != None:              
            fig = plot_rps(df, option)
            st.pyplot(fig)
        goShort = setupAGChart(df,api_columns)
        response = AgGrid(
            df, gridOptions=goShort, custom_css=custom_css_prd, allow_unsafe_jscode=True
        )

        selected = response["selected_rows"]
        if selected:
            st.markdown(
                '<p class="font1">  Selected rows :  </p>', unsafe_allow_html=True
            )
            AgGrid(convert_to_dataframe(selected), gridOptions=goShort)
            sjl = st.button("Show Job Logs")
            if sjl:
                st.markdown(
                    '<p class="font1">  JSON Logs :  </p>', unsafe_allow_html=True
                )
                st.write(getJobLogsForCorrelationID(selected,st.session_state.file_path ,"API"))
        slt1, slt2, slt3 = st.columns([2, 2, 6])
        top_type = slt1.selectbox(" ", ['Project','Path', 'User', 'Puma Worker', 'Route'], label_visibility = "collapsed")
        top_filter = slt2.selectbox(" ", ["Duration", "Memory", "DB Duration", "CPU"], label_visibility = "collapsed")
        dt = pd.json_normalize(getTopInfoAPI(df, top_type, top_filter))
        st.markdown(
            '<p class="font1"> Top 10 {} by {} </p>'.format(top_type, top_filter),
            unsafe_allow_html=True,
        )
        go = setupSmallAGChart(dt)
        AgGrid(dt,gridOptions=go, custom_css=custom_css_prd, allow_unsafe_jscode=True)
        st.divider()
    else:
        filePathExists()

def process_file(file_):
    st.spinner(text="File uploaded, processing...")
    file__ = process_file(file_)
    return file__

def indexPage():
    c1, c2 = st.columns([6.5,3.5])
    with c1:
        st.markdown(
            " This tool uses *Streamlit & Pandas* libraries to parse and display the logs on the page."
        )
        st.markdown(
            "For more information on the tool, please refer to the project : [SOSParser](https://gitlab.com/gitlab-com/support/toolbox/sosparser)"
        )
        st.markdown(   "---")
        st.markdown(
            '<p class="font2">Input log details or select from previous entries</p>',
            unsafe_allow_html=True,
        )
        stb = st.text_input("Log directory path", key="path", placeholder="")
        stb1 = st.text_input("Comment (Optional) :", key="comment", placeholder="")
        if st.query_params != {}:
            cli_args = st.query_params
            c2.empty()
            if validateFilepath(cli_args['path'][0], c2):
#                saveLogEntry(cli_args['path'][0], cli_args['comment'][0])
                st.session_state.valid = True
                c2.markdown("---")
                c2.markdown(
                    ":arrow_backward: Select the page from the dropdown menu in the side bar "
                )
                st.session_state.file_path = cli_args['path'][0]
                c2.success("This is a success message!", icon="✅")
            else:
                c2.error("Something went wrong, please check file path", icon="🔥")
        if st.button("Submit"):
            with st.spinner(text="Validating the folder content, processing..."):
                c2.empty()
                if validateFilepath(stb, c2):
                    saveLogEntry(stb, stb1)
                    st.session_state.valid = True
                    c2.markdown("---")
                    c2.markdown(
                        ":arrow_backward: Select the page from the dropdown menu in the side bar "
                    )
                    st.session_state.file_path = stb
                    c2.success("This is a success message!", icon="✅")
                else:
                    c2.error("Something went wrong, please check file path", icon="🔥")
        st.markdown(
            '<p class="font2">Last 10 log files</p>',
            unsafe_allow_html=True,
        )
        logHistory = showLogHistory()
        #st.table(logHistory)
        showTabls(logHistory,c2)

def initialize():
    st.set_page_config(
        page_title="SOS Parser",
        page_icon="\U0001F6A8",
        layout="wide",
        initial_sidebar_state="expanded",
    )
    if "file_path" not in st.session_state:
        st.session_state["file_path"] = ""
    if "valid" not in st.session_state:
        st.session_state["valid"] = False
    hide_table_row_index = """
                <style>
                thead tr th:first-child {display:none}
                tbody th {display:none}
                </style>
                """
    st.markdown(
        """
            <style>
                .css-18e3th9 {
                        padding-top: 0rem;
                        padding-bottom: 10rem;
                        padding-left: 5rem;
                        padding-right: 5rem;
                    }
                .css-1d391kg {
                        padding-top: 3.5rem;
                        padding-right: 1rem;
                        padding-bottom: 3.5rem;
                        padding-left: 1rem;
                    }
            </style>
            """,
        unsafe_allow_html=True,
    )
    st.markdown(
        """
    <style>
    div[data-testid="metric-container"] > label[data-testid="stMetricLabel"] > div {
    overflow-wrap: break-word;
    white-space: break-spaces;
    color: green;
    }
    div[data-testid="metric-container"] > label[data-testid="stMetricLabel"] > div p {
    font-size: 120% !important;
    }
    </style>
    """,
        unsafe_allow_html=True,
    )
    return True

def logo(text):

    cl1,cl2,cl3 = st.columns([10,10,5])
    st.write(
        "<style>div.block-container{padding-top:0rem;}</style>",
        unsafe_allow_html=True,
    )
    cl1.title(":orange["+text+"]")
    image = Image.open('static/gitlab_logo.png')
    cl3.image(image,width=300)

def versionMainfestPage():
    if st.session_state.valid:
        data_ = getManifestVersions(st.session_state.file_path)
        cm = st.columns([1, 1, 1, 1, 1, 1,1,1])
        metadata = importantVersions(data_)
        for x, meta_in in enumerate(metadata.keys()):
            cm[x].metric(":green["+meta_in+"]", metadata[meta_in])
        st.table(data_)
        return True
    else:
        filePathExists()

def workhorsePage():
    if st.session_state.valid:
        df = getWorkhorseLogs(st.session_state.file_path)
        df = mapColumnsWH(df)
        cm = st.columns([1, 1, 1, 1, 1, 1, 1])
        metadata = workhorse_metrics(df)
        for x, meta_in in enumerate(metadata.keys()):
            cm[x].metric(":green["+meta_in+"]", metadata[meta_in])
        st.markdown(':red[Select timeframe :]', unsafe_allow_html=True)
        option = st.selectbox(" ", ['1 Second','5 Seconds','10 Seconds','30 Seconds', '1 Minute', '5 Minutes', '10 Minutes'],index=1,label_visibility = "collapsed")
        if option != None:              
            fig = plot_rps(df, option)
            st.pyplot(fig)
        st.subheader(":orange[Workhorse log table]")
        goShort = setupAGChart(df,wh_path)
        response = AgGrid(
            df, gridOptions=goShort, custom_css=custom_css_prd, allow_unsafe_jscode=True
        )
        st.divider()
        st.subheader(":orange[Errors and warnings]")
        col1, col2 = st.columns(2)
        with col1:
            df_ =workhorse_status_codes(df)
            st.dataframe(df_,height=400)
        with col2:
            df_ = workhorse_errors(df)
            st.dataframe(df_, height=400)
        st.divider()
        col1, col2 = st.columns(2)
        with col1:
            st.subheader(":orange[Request Method Distribution]")
            st.bar_chart(df['method'].value_counts())
        with col2:
            st.subheader(":orange[Top endpoints]")
            st.bar_chart(df['uri'].value_counts().head(10))
        st.divider()
        col1, col2 = st.columns(2)
        with col1:
            st.subheader(":orange[Requests Per Second Over Time]")
            df['time'] = pd.to_datetime(df['time'], errors='coerce')
            df_rps = df.set_index('time').resample('1Min').size().reset_index(name='count')
            df_rps['rps'] = df_rps['count'] / 60
            st.line_chart(df_rps.set_index('time')['rps'])
        with col2:
            st.subheader(":orange[Status Code Distribution]")
            st.bar_chart(df['status'].value_counts())
        st.divider()
        st.subheader(f":orange[Requests by IP address]")
        col1, col2 = st.columns([8,2])
        ip_counts = df['remote_ip'].value_counts()
        chart_data = pd.DataFrame({'IP Address': ip_counts.index, 'Requests': ip_counts.values})
        with col1:
            chart_data = chart_data.sort_values('Requests', ascending=True)
            st.bar_chart(chart_data.set_index('IP Address'), height=400)
        with col2:
            chart_data = chart_data.sort_values('Requests', ascending=False)
            st.dataframe(chart_data, height=400)
    else:
        filePathExists()

def filePathExists():
    st.markdown("#### Please provide a valid path to the logs directory")

def logFileNotFound(type_):
    st.markdown('<p class="font1"> \U0001F6A8 The {} log file was not found. Maybe the logs are not collected from {} node. </p>'.format(type_, type_), unsafe_allow_html=True)    

def showTabls(log_history, c2):
    colms = st.columns((0.5, 2, 2, 6,1))
    fields = ["#", 'Timestamp', 'Comment', 'Path']
    for col, field_name in zip(colms, fields):
        # header
        col.write(field_name)
    for x, entry in enumerate(log_history):
        col1, col2, col3, col4, col5 = st.columns((0.5, 2, 2, 6,1))
        col1.write(x)  # index
        col2.write(entry['Timestamp'])  
        col3.write(entry['Comment']) 
        col4.write(entry['Path'])  
        if col5.button("Load", key=x):
            if validateFilepath(entry['Path'], c2):
                st.session_state.valid = True
                st.session_state.file_path = entry['Path']
                c2.success("Successfully loaded the logs", icon="✅")
                c2.markdown("---")
                c2.markdown(
                    ":arrow_backward: Select the page from the dropdown menu in the side bar "
                )
            else:
                c2.error("Something went wrong, please check file path", icon="🔥")

def auditEvents():
    if st.session_state.valid:
        df_logs, df_errors = getAuditEvents(st.session_state.file_path)
        st.markdown(
            '<p class="font2">Entries from the Audit event log file :</p>',
            unsafe_allow_html=True,
        )
        goShort = setupAGChart(df_logs, [])
        response = AgGrid(
            df_logs, gridOptions=goShort, custom_css=custom_css_prd, allow_unsafe_jscode=True
        )
        st.markdown("---")
        goShort = setupSmallAGChart(df_errors)
        AgGrid(
            df_errors, gridOptions=goShort, custom_css=custom_css_prd, allow_unsafe_jscode=True, key = "Audit Event")
    else:
        filePathExists()

def display_custom_table(df):
    styled_df = style_dataframe(df)
    st.dataframe(styled_df, use_container_width=True)

if __name__ == "__main__":
    main()
